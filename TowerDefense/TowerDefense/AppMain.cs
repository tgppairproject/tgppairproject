using System;
using System.Collections.Generic;
using System.Linq;

using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Environment;
using Sce.PlayStation.Core.Graphics;
using Sce.PlayStation.Core.Input;
using Sce.PlayStation.Core.Audio;

using Sce.PlayStation.HighLevel.GameEngine2D;
using Sce.PlayStation.HighLevel.GameEngine2D.Base;
using Sce.PlayStation.HighLevel.UI;

using System.Diagnostics;
	
namespace TowerDefense
{
	public enum States
	{
   		Intro,
    	MainMenu,
    	Settings,
		Game,
		HighScore,
		Credits,
		Tutorial,
		GameOver,
		LevelSelect
	}
	
	public struct towerStats
	{
		public string graphicPath { get; set; }
		public string title { get; set; }
		public string description { get; set; }
		public int damage { get; set; }
		public float range { get; set; }
		public float splash { get; set; }
		public int rateOfFire { get; set; }
		public int cost { get; set; }
		
		public towerStats(string gP, string t, string de, int d, float r, float s, int rof, int c) : this()
		{
			graphicPath = gP;
			title = t;
			description = de;
			damage = d;
			range = r;
			splash = s;
			rateOfFire = rof;
			cost = c;
		}
	}
	
	public struct creepStats
	{
		public string graphicPath { get; set; }
		public string title { get; set; }
		public string description { get; set; }
		public float maxHealth { get; set; }
		public float health;
		public float maxSpeed { get; set; }
		public float speedModifier;
		public float gold { get; set; }
		
		public creepStats(string gP, string t, string de, float mh, float h, float mS, float sM, float g) : this()
		{
			graphicPath = gP;
			title = t;
			description = de;
			maxHealth = mh;
			health = h;
			maxSpeed = mS;
			speedModifier = sM;
			gold = g;
		}
		
		public float getHealth()
		{
			return health;
		}
		
		public void setHealth(float h)
		{
			health = h;
		}
		
		public float getSpeedModifier()
		{
			return speedModifier;
		}
		
		public void setSpeedModifier(float s)
		{
			speedModifier = s;	
		}
	}
	
	public class AppMain
	{
		private static Sce.PlayStation.HighLevel.GameEngine2D.Scene 	gameScene;
		private static Sce.PlayStation.HighLevel.UI.Scene 				uiScene;
		private static Sce.PlayStation.HighLevel.UI.Scene 				gameOverScene;

		// [DL] Member Variables go here
		private static List<BaseTower> towers = new List<BaseTower>();
		private static List<BaseCreep> creeps = new List<BaseCreep>();
		private static List<BaseProjectile> projectiles = new List<BaseProjectile>();
		private static List<BulletParticle> bulletParticles = new List<BulletParticle>();
		private static List<EMPParticle> empParticles = new List<EMPParticle>();
		private static List<IncendiaryParticle> incendiaryParticles = new List<IncendiaryParticle>();
		
		// [DL] Map Variables
		public static float gridSize { get {return gridSize; } set { gridSize = value; } }
		private static int[] selectedGridCoords;
		private static int wave = 0;
		public static int speed = 3; // [DL] gameSpeed, 0 being no activeCreeps
		private static int mapLevel = 150; // [DL] 0= Tutorial, 1= Easy, 2= Medium, 3= Hard
		private static Random r = new Random();
		private static int numberOfWaves;
		
		// [DL] Splash waypoint data
		private static Vector2[] splashWaypoints = {new Vector2(125.0f,644.0f),new Vector2(125.0f,125.0f),new Vector2(835.0f,125.0f),new Vector2(835.0f,644.0f)}; // [DL] Hard Waypoints: Path 1
		private static int[] splashSpawns = {9000,5,5,5,5,5}; // [DL] Splash screen wave
									    
				// [DL] Easy & tutorial wp data
		// [DL] Int arrays hold x/y coordinates, level initializer combines into vector 2 arrays
		private static int[] easyWPx = {-100,675,675,125,125,850};
		private static int[] easyWPy = {375,375,225,225,75,75};
		private static Vector2[] tutorialWaypoints; // [DL] Tutorial Waypoints
		private static Vector2[] easyWaypoints; // [DL] Easy Waypoints
		
		// [DL] Medium wp data
		// [DL] Int arrays hold x/y coordinates, level initializer combines into vector 2 arrays
		private static int[] mediumWP1x = {-100,125,125,325,325,425,425,575,575,725,725,850};
		private static int[] mediumWP1y = {175,175,125,125,175,175,75,75,175,175,125,125};
		private static int[] mediumWP2x = {-100,125,125,325,325,425,425,575,575,725,725,850};
		private static int[] mediumWP2y = {325,325,375,375,325,325,425,425,325,325,375,375};
		private static Vector2[] mediumWaypoints1; // [DL] Medium Waypoints
		private static Vector2[] mediumWaypoints2; // [DL] Medium Waypoints
		
		// [DL] Hard wp data. straight paths means fewer wps, so no need for int arrays and initializer
		private static Vector2[] hardWaypoints1 = {new Vector2(-100.0f,325.0f),new Vector2(850.0f,325.0f)}; // [DL] Hard Waypoints: Path 1
		private static Vector2[] hardWaypoints2 = {new Vector2(-100.0f,175.0f),new Vector2(850.0f,175.0f)}; // [DL] Hard Waypoints: Path 2
		private static Vector2[] hardWaypoints3 = {new Vector2(325.0f,625.0f),new Vector2(325.0f,-50.0f)}; // [DL] Hard Waypoints: Path 3
		private static Vector2[] hardWaypoints4 = {new Vector2(475.0f,625.0f),new Vector2(475.0f,-50.0f)}; // [DL] Hard Waypoints: Path 4
		
		private static Vector2[] wps; // [DL] Depending on level and path randomisation, waypoints get set here, and this gets passed into creeps
		
		// [DL] gridStatus : Multifunctionality, UI Display, stats display, upgrading, selling and creating
		// [DL] gridStatus (-1=unusable, 0=emptyTower, 1=emptyRoad, 2=autocannonStats, 3=autocannon, 4=heavyArtilleryStats, 5=heavyArtillery, 6=mudStats
		// [DL] gridStatus (7=mud, 8=barbedWireStats, 9=barbedWire, 10=electricFenceStats, 11=electricFence, 12=sniperStats, 13=sniper, 14=chaingunStats
		// [DL] gridStatus (15=chaingun, 16=rocketLauncherStats, 17=rocketLauncher, 18=missileLauncherStats, missileLauncher
		private static int[,] gridStatus;
		
		private static int[,] tutorialGrid = {{0,0,0,0,0,0,0,1,0,0}, // [DL] : [y,z] Left Column, Bottom to Top (for [x,y] simplicity in accessors)
										      {0,0,0,0,0,0,0,1,0,0},
										  	  {0,1,1,1,1,0,0,1,0,0},
										 	  {0,1,0,0,1,0,0,1,0,0},
										 	  {0,1,0,0,1,0,0,1,0,0},
										  	  {0,1,0,0,1,0,0,1,0,0},
										 	  {0,1,0,0,1,0,0,1,0,0},
										 	  {0,1,0,0,1,0,0,1,0,0},
										  	  {0,1,0,0,1,0,0,1,0,0},
									      	  {0,1,0,0,1,0,0,1,0,0},
										  	  {0,1,0,0,1,0,0,1,0,0},
										 	  {0,1,0,0,1,0,0,1,0,0},
										 	  {0,1,0,0,1,0,0,1,0,0},
										 	  {0,1,0,0,1,1,1,1,0,0},
										 	  {0,1,0,0,0,0,0,0,0,0},
										 	  {0,1,0,0,0,0,0,0,0,0}};
		
		private static int[,] easyGrid = {{0,0,0,0,0,0,0,1,0,0}, // [DL] : [y,z] Left Column, Bottom to Top (for [x,y] simplicity in accessors)
									      {0,0,0,0,0,0,0,1,0,0},
									  	  {0,1,1,1,1,0,0,1,0,0},
									 	  {0,1,0,0,1,0,0,1,0,0},
									 	  {0,1,0,0,1,0,0,1,0,0},
									  	  {0,1,0,0,1,0,0,1,0,0},
									 	  {0,1,0,0,1,0,0,1,0,0},
									 	  {0,1,0,0,1,0,0,1,0,0},
									  	  {0,1,0,0,1,0,0,1,0,0},
								      	  {0,1,0,0,1,0,0,1,0,0},
									  	  {0,1,0,0,1,0,0,1,0,0},
									 	  {0,1,0,0,1,0,0,1,0,0},
									 	  {0,1,0,0,1,0,0,1,0,0},
									 	  {0,1,0,0,1,1,1,1,0,0},
									 	  {0,1,0,0,0,0,0,0,0,0},
									 	  {0,1,0,0,0,0,0,0,0,0}};
		
		private static int[,] mediumGrid = {{0,0,0,1,0,0,1,0,0,0}, // [DL] : [y,z] Left Column, Bottom to Top (for [x,y] simplicity in accessors)
										    {0,0,0,1,0,0,1,0,0,0},
										  	{0,0,1,1,0,0,1,1,0,0},
										 	{0,0,1,0,0,0,0,1,0,0},
										 	{0,0,1,0,0,0,0,1,0,0},
										  	{0,0,1,0,0,0,0,1,0,0},
										 	{0,0,1,1,0,0,1,1,0,0},
										 	{0,0,0,1,0,0,1,0,0,0},
										  	{0,1,1,1,0,0,1,1,1,0},
									      	{0,1,0,0,0,0,0,0,1,0},
										  	{0,1,0,0,0,0,0,0,1,0},
										 	{0,1,1,1,0,0,1,1,1,0},
										 	{0,0,0,1,0,0,1,0,0,0},
										 	{0,0,0,1,0,0,1,0,0,0},
										 	{0,0,1,1,0,0,1,1,0,0},
										 	{0,0,1,0,0,0,0,1,0,0}};
		
		private static int[,] hardGrid = {{0,0,0,1,0,0,1,0,-1,-1}, // [DL] :  [y,z] Left Column, Bottom to Top (for [x,y] simplicity in accessors)
										  {0,0,0,1,0,0,1,-1,-1,-1},
										  {0,0,0,1,0,0,1,0,-1,-1},
										  {0,0,0,1,0,0,1,0,-1,-1},
										  {0,0,0,1,0,0,1,0,0,-1},
										  {0,0,0,1,0,0,1,0,0,0},
										  {1,1,1,1,1,1,1,1,1,1},
										  {0,0,0,1,0,0,1,0,0,0},
										  {0,0,0,1,0,0,1,0,0,0},
									      {1,1,1,1,1,1,1,1,1,1},
										  {0,0,0,1,0,0,1,0,0,0},
										  {0,0,0,1,0,0,1,0,0,0},
										  {0,0,0,1,0,0,1,0,0,0},
										  {0,0,0,1,0,0,1,0,0,0},
										  {0,0,0,1,0,0,1,0,0,0},
										  {0,0,0,1,0,0,1,0,0,0}};
	
		// [DL] Creep Variables 
		
		// [DL] Spawns : {Time in seconds for all of wave to spawn,Militants, Canines, APC, Chinook, Tank}
		static private int[,,] spawns = {{{300,2,0,0,0,0}, // [DL] Tutorial Wave 1
									      {300,0,2,0,0,0},
			 							  {60,0,0,1,0,0},
										  {60,0,0,0,1,0},
										  {60,0,0,0,0,1} // [DL] Tutorial Wave 5
									     },
									     {{600,3,2,0,0,0}, // [DL] EasyWave 1
										  {600,2,2,1,0,0},
										  {600,2,2,0,1,0},
										  {600,0,0,1,1,0},
										  {60,0,0,0,0,1} // [DL] Easy Wave 5
										 },
										 {{600,15,7,3,2,5}, // [DL] MediumWave 1
										  {600,2,2,1,0,0},
										  {600,2,2,0,1,0},
										  {600,0,0,1,1,0},
										  {60,0,0,0,0,1} // [DL] MediumWave 5
										 },
										 {{3000,15,7,3,2,5}, // [DL] hardWave 1
										  {600,2,2,1,0,0},
										  {600,2,2,0,1,0},
										  {600,0,0,1,1,0},
										  {60,0,0,0,0,1} // [DL] hardWave 1
									    }};
		
		private static List<BaseCreep> creepQueue = new List<BaseCreep>();
		
		// [DL] Scoring Variables
		private const int maxLives = 10;
		private static int lives;
		private static float gold;
		private static float score = 0;
		private static int creepsKilled = 0;
		private static int creepsEscaped = 0;
		
		// [DL] Game State Variables
		private static States gameState;
		private static int gameUIState = -1;
		private static int touchCooldown = 0;
		
		// [DL] Image Variables
		private static SpriteUV mapSprite;
		private static TextureInfo mapTextureInfo;
		
		private static SpriteUV interfaceSprite;
		private static TextureInfo interfaceTextureInfo;
		
		private static SpriteUV rangeSprite;
		private static TextureInfo rangeTextureInfo;
		
		private static SpriteUV targetSprite;
		private static TextureInfo targetTextureInfo;
		
		private static SpriteUV startWaveSprite;
		private static TextureInfo startWaveTextureInfo;
		
		private static SpriteUV speedUpSprite;
		private static TextureInfo speedUpTextureInfo;
		
		private static SpriteUV slowDownSprite;
		private static TextureInfo slowDownTextureInfo;
		
		// [DL] Label Variables
		private static Sce.PlayStation.HighLevel.UI.Label scoreLabel;
		private static Sce.PlayStation.HighLevel.UI.Label waveLabel;
		private static Sce.PlayStation.HighLevel.UI.Label goldLabel;
		private static Sce.PlayStation.HighLevel.UI.Label creepsKilledLabel;
		private static Sce.PlayStation.HighLevel.UI.Label creepsEscapedLabel;
		private static Sce.PlayStation.HighLevel.UI.Label finalScoreLabel;
		
		
		// [DL] Constructor
		public static void Main (string[] args)
		{
			Initialize();
			
			// [RB] Play BGM through-out game on repeat
            Bgm music = new Bgm("/Application/sounds/TheForestAwakes.mp3"); // [RB] mp3 soundtrack open source, creative commons, tannerhelland.com
            BgmPlayer mp3Player = music.CreatePlayer();
            mp3Player.Play();
            mp3Player.Loop = true;
			// [RB] tower firing sound effect - from wavsource.com
			Sound soundProjectile;
			SoundPlayer soundPlayerProjectile;
			soundProjectile = new Sound("/Application/sounds/cannon_x.wav");
			soundPlayerProjectile = soundProjectile.CreatePlayer();
			// [RB] collision explosion sound effect - from wavsource.com
			Sound soundExplosion;
			SoundPlayer soundPlayerExplosion;
			soundExplosion = new Sound("/Application/sounds/explosion_x.wav");
			soundPlayerExplosion = soundExplosion.CreatePlayer();
			
			bool quitGame = false;
			
			while (!quitGame) 
			{
				Update ();
				
				Director.Instance.Update();
				Director.Instance.Render();
				UISystem.Render();
				
				Director.Instance.GL.Context.SwapBuffers();
				Director.Instance.PostSwap();
			}
			
			//Clean up after ourselves.
			//Dispose code goes here
			
			Director.Terminate ();
		}

		public static void Initialize ()
		{
			//Set up director and UISystem.
			Director.Initialize ();
			UISystem.Initialize(Director.Instance.GL.Context);
			
			//Set game scene
			gameScene = new Sce.PlayStation.HighLevel.GameEngine2D.Scene();
			gameScene.Camera.SetViewFromViewport();
			
			//Set the ui scene.
			uiScene = new Sce.PlayStation.HighLevel.UI.Scene();
			Panel panel  = new Panel();
			panel.Width  = Director.Instance.GL.Context.GetViewport().Width;
			panel.Height = Director.Instance.GL.Context.GetViewport().Height;
						
			uiScene.RootWidget.AddChildLast(panel);
			UISystem.SetScene(uiScene);
			uiScene.Visible = false;
			
			scoreLabel = new Sce.PlayStation.HighLevel.UI.Label();
			scoreLabel.SetPosition(20,8);
			scoreLabel.Text = "Score: " + score.ToString();
			panel.AddChildLast(scoreLabel);
			
			waveLabel = new Sce.PlayStation.HighLevel.UI.Label();
			waveLabel.SetPosition(827,8);
			waveLabel.Text = "Wave: " + wave.ToString() + "/" + numberOfWaves.ToString();
			panel.AddChildLast(waveLabel);
			
			goldLabel = new Sce.PlayStation.HighLevel.UI.Label();
			goldLabel.SetPosition(827,54);
			goldLabel.Text = "Gold: " + gold.ToString();
			panel.AddChildLast(goldLabel);
			
			//Set the gameOver scene.
			gameOverScene = new Sce.PlayStation.HighLevel.UI.Scene();
			Panel panel2  = new Panel();
			panel2.Width  = Director.Instance.GL.Context.GetViewport().Width;
			panel2.Height = Director.Instance.GL.Context.GetViewport().Height;
			
			gameOverScene.RootWidget.AddChildLast(panel2);
			gameOverScene.Visible = false;
			
			creepsKilledLabel = new Sce.PlayStation.HighLevel.UI.Label();
			creepsKilledLabel.SetPosition(350,290);
			creepsKilledLabel.Text = "Creeps Killed: " + creepsKilled.ToString();
			panel2.AddChildLast(creepsKilledLabel);
			
			creepsEscapedLabel = new Sce.PlayStation.HighLevel.UI.Label();
			creepsEscapedLabel.SetPosition(350,335);
			creepsEscapedLabel.Text = "Creeps Escaped: " + creepsKilled.ToString();
			panel2.AddChildLast(creepsEscapedLabel);
			
			finalScoreLabel = new Sce.PlayStation.HighLevel.UI.Label();
			finalScoreLabel.SetPosition(350,380);
			finalScoreLabel.Text = "Final Score: " + score.ToString();
			panel2.AddChildLast(finalScoreLabel);
			
			//Run the scene.
			Director.Instance.RunWithScene(gameScene, true);
						
			// [DL] Sets gamestate to intro, and image to splash screen
			States gameState = States.Intro;
			
			interfaceTextureInfo = new TextureInfo("/Application/textures/splash.png");
			interfaceSprite = new SpriteUV(interfaceTextureInfo);
			interfaceSprite.Scale = new Vector2(Director.Instance.GL.Context.GetViewport ().Width, Director.Instance.GL.Context.GetViewport ().Height);
			gameScene.AddChild(interfaceSprite);
			
			// [DL] Set up splash screen variables
			setUpSpawnWave();
		}
		
		public static void addCreepKilled()
		{
			creepsKilled++;	
		}
		
		public static void addCreepEscaped()
		{
			creepsEscaped++;	
		}
		
		public static int getSpeed()
		{
			return speed;
		}
		
		public static void updateScore()
		{
			scoreLabel.Text = "Score: " + score.ToString();	
			waveLabel.Text = "Wave: " + (wave+1).ToString() + "/" + numberOfWaves.ToString();
			goldLabel.Text = "Gold: " + gold.ToString();
		}
		
		public static void setUpSpawnWave()
		{			
			// [DL] Create some of each tower
			
			// [DL] Muds just above each corner
			Mud m1 = new Mud(gameScene, new Vector2(100f,300f));
			towers.Add(m1);
			Mud m2 = new Mud(gameScene, new Vector2(810f,300f));
			towers.Add(m2);
			
			// [DL] EMP at left corner, barbedWire at right corner
			EMP e = new EMP(gameScene, new Vector2(100f,100f));
			towers.Add(e);
			BarbedWire b = new BarbedWire(gameScene, new Vector2(810f,100f));
			towers.Add(b);
			
			// [DL] Autocannon in inside corners
			AutoCannon c1 = new AutoCannon(gameScene, new Vector2(170f,170f));
			towers.Add (c1);
			AutoCannon c2 = new AutoCannon(gameScene, new Vector2(740f,170f));
			towers.Add (c2);
			
			// [DL] Missile Launcher near center left, and center right
			MissileLauncher ml1 = new MissileLauncher(gameScene, new Vector2(250f,250f));
			towers.Add (ml1);
			MissileLauncher ml2 = new MissileLauncher(gameScene, new Vector2(660f,250f));
			towers.Add (ml2);
			
			// [DL] Rocket Launchers at outside corners
			RocketLauncher rl1 = new RocketLauncher(gameScene, new Vector2(30f,30f));
			towers.Add (rl1);
			RocketLauncher rl2 = new RocketLauncher(gameScene, new Vector2(880f,30f));
			towers.Add (rl2);
			
			// [DL] Snipers at left and right of tap screen writing
			Sniper sp1 = new Sniper(gameScene, new Vector2(250f,30f));
			towers.Add (sp1);
			Sniper sp2 = new Sniper(gameScene, new Vector2(660f,30f));
			towers.Add (sp2);
			
			// [DL] Chainguns near path start of left and right
			Chaingun cg1 = new Chaingun(gameScene, new Vector2(170f,330f));
			towers.Add (cg1);
			Chaingun cg2 = new Chaingun(gameScene, new Vector2(740f,330f));
			towers.Add (cg2);
			
			// [DL] Finally, create spawns
			queueNextWave();
		}
		
		private static void sceneObjectsUpdate()
		{
			// [DL] Spawn creeps in creepQueue
			spawnCreeps();
			
			// [DL] Update creep movement
			for(int i = 0; i<speed; i++)
			{
				foreach(BaseCreep j in creeps)
				{
					j.updateMovement(gameScene);
				}
				
				foreach(BaseTower k in towers)
				{
					k.mainUpdate(gameScene, creeps);
				}
				
				foreach(BaseProjectile l in projectiles)
				{
					l.updateMovement(gameScene);
				}
				
				foreach(IncendiaryParticle p in incendiaryParticles)
				{
					p.UpdateState(gameScene);
				}
				
				// [DL] Create temporary list for creeps who are dead or finished moving through waypoints, and remove them from creeps
				var selected = creeps.Where(BaseCreep => BaseCreep.isDead == true).ToList();
				selected.ForEach(BaseCreep => creepQueue.Remove(BaseCreep));
				while(selected.Count() > 0)
				{
					selected.First().cleanup(gameScene);
					creeps.Remove(selected.First());
					selected.Remove(selected.First());
				}	
				
				// [DL] Create temporary list for projectiles who have hit their target, and initiate them
				var tempList = projectiles.Where(BaseProjectile => BaseProjectile.hasHit == true).ToList();
				tempList.ForEach(BaseProjectile => projectiles.Remove(BaseProjectile));
				while(tempList.Count() > 0)
				{
					tempList.First().impact(gameScene, creeps);
					projectiles.Remove(tempList.First());
					tempList.Remove(tempList.First());
				}
				
				// [DL] Create temporary list for particles which have finished their process, then remove them from particles
				var tempList2 = incendiaryParticles.Where(IncendiaryParticle => IncendiaryParticle.hasFinished == true).ToList();
				tempList2.ForEach(IncendiaryParticle => incendiaryParticles.Remove(IncendiaryParticle));
				while(tempList2.Count() > 0)
				{
					incendiaryParticles.Remove(tempList2.First());
					tempList2.Remove(tempList2.First());
				}	
				
				// [DL] If ingame and no more creeps, change speed and show correct images
				if(mapLevel!=150 && creeps.Count() == 0 && creepQueue.Count() == 0)
				{
					// [DL] wave finished, clear any remaining particles and projectiles and increase wave count
					foreach(IncendiaryParticle p in incendiaryParticles)
					{
						p.cleanUp(gameScene);
					}
					
					foreach(BaseProjectile p in projectiles)
					{
						p.partialCleanup(gameScene);
					}
					
					speed=0;
					projectiles = new List<BaseProjectile>();
					incendiaryParticles = new List<IncendiaryParticle>();
					updateInterface();
					wave++;
					
					if(wave>=numberOfWaves)
					{
						// [DL] No more waves, gameOver	
					}
				}
			}
		}
		
		public static void Update()
		{
			// [DL] Decrease touchcooldown
			if(touchCooldown>0)
			{
				touchCooldown--;	
			}			
			
			switch(gameState)
    		{
        		case States.Intro:
            		ProcessIntroState();
            		break;
        		case States.MainMenu:
            		ProcessMainMenuState();
            		break;
        		case States.Settings:
            		ProcessSettingsState();
            		break;
				case States.Game:
					sceneObjectsUpdate();
            		ProcessGameState();
            		break;
				case States.HighScore:
            		ProcessHighScoreState();
            		break;
				case States.Credits:
            		ProcessCreditsState();
            		break;
				case States.Tutorial:
            		ProcessTutorialState();
            		break;
				case States.GameOver:
            		ProcessGameOverState();
            		break;
				case States.LevelSelect:
            		ProcessLevelSelectState();
            		break;
    		}			
		}
		
		public static Random getRandom()
		{
			return r;	
		}
		
		public static void addToScore(int s)
		{
			score += s;
		}
		
		public static void removeFromScore(int s)
		{
			score -= s;
		}
				
		public static void ResetGame()
		{		
			// [DL] Remove any game sprites
			gameScene.RemoveChild(mapSprite,true);
			gameScene.RemoveChild(interfaceSprite,true);
			
			foreach(IncendiaryParticle p in incendiaryParticles)
			{
				p.cleanUp(gameScene);
			}	
			
			foreach(BaseProjectile p in projectiles)
			{
				p.partialCleanup(gameScene);
			}			
			
			foreach(BaseTower t in towers)
			{
				t.sell(gameScene);
			}
			
			foreach(BaseCreep c in creeps)
			{
				c.cleanup(gameScene);
			}
			
			// [DL] Reinitialise lists
			towers = new List<BaseTower>();
			creeps = new List<BaseCreep>();
			creepQueue = new List<BaseCreep>();
			projectiles = new List<BaseProjectile>();
			incendiaryParticles = new List<IncendiaryParticle>();
			
			//lives = maxLives;
			speed = 0;
			uiScene.Visible = false;
			wave = 0;
			mapLevel = 150;
			touchCooldown=15;
		}
		
		public static Boolean withdrawGold(int cost)
		{
			if(cost<=gold)
			{
				gold -= cost;
				return true;
			}
			else
			{
				return false;	
			}
		}
		
		public static void depositGold(int deposit)
		{
			gold += deposit;	
		}
		
		// [DL] Randomize Waypoints
		public static void randomizeWaypoints()
		{
			wps = tutorialWaypoints; // [DL] Set to tutorialWaypoints as initialisation
			
			if (mapLevel==0)
			{
				// [DL] Tutorial Level
				wps = tutorialWaypoints;
			}
			else if (mapLevel==1)
			{
				// [DL] First Level (Easy)
				wps = easyWaypoints;
			}
			else if (mapLevel==2)
			{
				// [DL] Second Level (Medium)
				int rand = r.Next(1, 3);
				
				if(rand==1)
				{
					wps = mediumWaypoints1; // [DL] Hard Path 1	
				}
				else
				{
					wps = mediumWaypoints2; // [DL] Hard Path 2	
				}
			}
			else if (mapLevel==3)
			{
				// [DL] Third Level (Hard)
				int rand = r.Next(1, 5);
				
				if(rand==1)
				{
					wps = hardWaypoints1; // [DL] Hard Path 1	
				}
				else if(rand==2)
				{
					wps = hardWaypoints2; // [DL] Hard Path 2	
				}
				else if(rand==3)
				{
					wps = hardWaypoints3; // [DL] Hard Path 3
				}
				else
				{
					wps = hardWaypoints4; // [DL] Hard Path 4	
				}
			}
			else
			{
				wps = splashWaypoints; // [DL] Splash screen waypoints	
			}
		}
		
		// [DL] Method to queue next spawn wave
		private static void queueNextWave()
		{	
			// [DL] Check if ingame, or splashscreen(50)
			if(mapLevel!=150)
			{
				// [DL] Creates militant creeps, creating the amount as specified in the arrays int variable
				for(int i = 0; i<spawns[mapLevel,wave,1]; i++)
				{
					// [DL] Randomize Waypoints
					randomizeWaypoints();
					
					// [DL] Spawn timer
					int spawnTimer = r.Next(1,spawns[mapLevel,wave,0]);
					
					creepQueue.Add(new Militant(gameScene, spawnTimer, mapLevel, wps)); // [DL] Adds the creep into the queue
				}
				
				// [DL] Creates canine creeps, creating the amount as specified in the arrays int variable
				for(int i = 0; i<spawns[mapLevel,wave,2]; i++)
				{
					// [DL] Randomize Waypoints
					randomizeWaypoints();
					
					// [DL] Spawn timer
					int spawnTimer = r.Next(1,spawns[mapLevel,wave,0]);
					
					creepQueue.Add(new Canine(gameScene, spawnTimer, mapLevel, wps)); // [DL] Adds the creep into the queue
				}
				
				// [DL] Creates APC creeps, creating the amount as specified in the arrays int variable
				for(int i = 0; i<spawns[mapLevel,wave,3]; i++)
				{
					// [DL] Randomize Waypoints
					randomizeWaypoints();
					
					// [DL] Spawn timer
					int spawnTimer = r.Next(1,spawns[mapLevel,wave,0]);
					
					creepQueue.Add(new APC(gameScene, spawnTimer, mapLevel, wps)); // [DL] Adds the creep into the queue
				}
				
				// [DL] Creates Chinook creeps, creating the amount as specified in the arrays int variable
				for(int i = 0; i<spawns[mapLevel,wave,4]; i++)
				{
					// [DL] Randomize Waypoints
					randomizeWaypoints();
					
					// [DL] Spawn timer
					int spawnTimer = r.Next(1,spawns[mapLevel,wave,0]);
					
					creepQueue.Add(new Chinook(gameScene, spawnTimer, mapLevel, wps)); // [DL] Adds the creep into the queue
				}
				
				// [DL] Creates Tank creeps, creating the amount as specified in the arrays int variable
				for(int i = 0; i<spawns[mapLevel,wave,5]; i++)
				{
					// [DL] Randomize Waypoints
					randomizeWaypoints();
					
					// [DL] Spawn timer
					int spawnTimer = r.Next(1,spawns[mapLevel,wave,0]);
					
					//Tank tank = new Tank(spawnTimer+0, mapLevel, wp);
					creepQueue.Add(new Tank(gameScene, spawnTimer, mapLevel, wps)); // [DL] Adds the creep into the queue
				}
			}
			else
			{
				// [DL] Creates militant creeps, creating the amount as specified in the arrays int variable
				for(int i = 0; i<splashSpawns[1]; i++)
				{
					// [DL] Randomize Waypoints
					randomizeWaypoints();
					
					// [DL] Spawn timer
					int spawnTimer = r.Next(1,splashSpawns[0]);
					
					creepQueue.Add(new Militant(gameScene, spawnTimer, mapLevel, wps)); // [DL] Adds the creep into the queue
				}
				
				// [DL] Creates canine creeps, creating the amount as specified in the arrays int variable
				for(int i = 0; i<splashSpawns[2]; i++)
				{
					// [DL] Randomize Waypoints
					randomizeWaypoints();
					
					// [DL] Spawn timer
					int spawnTimer = r.Next(1,splashSpawns[0]);
					
					creepQueue.Add(new Canine(gameScene, spawnTimer, mapLevel, wps)); // [DL] Adds the creep into the queue
				}
				
				// [DL] Creates APC creeps, creating the amount as specified in the arrays int variable
				for(int i = 0; i<splashSpawns[3]; i++)
				{
					// [DL] Randomize Waypoints
					randomizeWaypoints();
					
					// [DL] Spawn timer
					int spawnTimer = r.Next(1,splashSpawns[0]);
					
					creepQueue.Add(new APC(gameScene, spawnTimer, mapLevel, wps)); // [DL] Adds the creep into the queue
				}
				
				// [DL] Creates Chinook creeps, creating the amount as specified in the arrays int variable
				for(int i = 0; i<splashSpawns[4]; i++)
				{
					// [DL] Randomize Waypoints
					randomizeWaypoints();
					
					// [DL] Spawn timer
					int spawnTimer = r.Next(1,splashSpawns[0]);
					
					creepQueue.Add(new Chinook(gameScene, spawnTimer, mapLevel, wps)); // [DL] Adds the creep into the queue
				}
				
				// [DL] Creates Tank creeps, creating the amount as specified in the arrays int variable
				for(int i = 0; i<splashSpawns[5]; i++)
				{
					// [DL] Randomize Waypoints
					randomizeWaypoints();
					
					// [DL] Spawn timer
					int spawnTimer = r.Next(1,splashSpawns[0]);
					
					//Tank tank = new Tank(spawnTimer+0, mapLevel, wp);
					creepQueue.Add(new Tank(gameScene, spawnTimer, mapLevel, wps)); // [DL] Adds the creep into the queue
				}
			}
		}
		
		public static List<BaseCreep> getCreeps()
		{
			return creeps;	
		}
		
		public static void addProjectile(BaseProjectile p)
		{
			projectiles.Add(p);	
		}
		
		public static void removeProjectile(BaseProjectile p)
		{
			projectiles.Remove(p);	
		}
		
		public static void addIncendiaryParticle(IncendiaryParticle p)
		{
			incendiaryParticles.Add(p);	
		}
		
		public static void removeIncendiaryParticle(IncendiaryParticle p)
		{
			incendiaryParticles.Remove(p);	
		}
		
		public static void spawnCreeps()
		{
			if(creepQueue.Count>=1)
			{			
				// [DL] For all creeps in the queue, reduce their spawntimer by 1*speed
				foreach(var creep in creepQueue)
				{	
					creep.setSpawnTimer(creep.getSpawnTimer()-(3));
				}
				
				// [DL] Create temporary list for creeps whose spawn timer has hit 0, remove them from queue list, and add them to creep list
				var selected = creepQueue.Where(BaseCreep => BaseCreep.getSpawnTimer() <= 0).ToList();
				selected.ForEach(BaseCreep => creepQueue.Remove(BaseCreep));
				creeps.AddRange(selected);
			}
		}
		
		public static void updateInterface()
		{
			gameScene.RemoveChild(interfaceSprite,true);
			
			interfaceTextureInfo = new TextureInfo("/Application/textures/Interface/" + gameUIState.ToString() + ".png");
			interfaceSprite = new SpriteUV(interfaceTextureInfo); 	
			interfaceSprite.Scale = new Vector2(Director.Instance.GL.Context.GetViewport ().Width, Director.Instance.GL.Context.GetViewport ().Height);
		
			gameScene.AddChild(interfaceSprite);
			
			if(speed == 1)
			{
				// [DL] Game Speed is at 1. Hide spawn/slowDown image, show speedUp image
				gameScene.RemoveChild(startWaveSprite,true);
				gameScene.RemoveChild(slowDownSprite,true);
				
				speedUpTextureInfo = new TextureInfo("/Application/textures/speedUp.png");
				speedUpSprite = new SpriteUV(speedUpTextureInfo); 	
				speedUpSprite.Position = new Vector2(819f,24f);
				speedUpSprite.Scale = new Vector2(123f,39f);
				gameScene.AddChild(speedUpSprite);
			}
			else if(speed == 3)
			{
				// [DL] Game Speed is at 3. Hide spawn/speedUp image, show slowDown image
				gameScene.RemoveChild(startWaveSprite,true);
				gameScene.RemoveChild(speedUpSprite,true);
				
				slowDownTextureInfo = new TextureInfo("/Application/textures/slowDown.png");
				slowDownSprite = new SpriteUV(slowDownTextureInfo); 
				slowDownSprite.Position = new Vector2(819f,24f);
				slowDownSprite.Scale = new Vector2(123f,39f);
				gameScene.AddChild(slowDownSprite);
			}
			else
			{
				// [DL] Speed is 0 due to no active creeps. Hide speed images, show spawnWave image	
				gameScene.RemoveChild(slowDownSprite,true);
				gameScene.RemoveChild(speedUpSprite,true);
				
				startWaveTextureInfo = new TextureInfo("/Application/textures/startWave.png");
				startWaveSprite = new SpriteUV(startWaveTextureInfo); 
				startWaveSprite.Position = new Vector2(819f,24f);
				startWaveSprite.Scale = new Vector2(123f,39f);
				gameScene.AddChild(startWaveSprite);
			}
		}
		
		public static void ProcessIntroState()
		{			
			// Check if tapped on screen, and if so, proceed to menu
			if(Touch.GetData (0).Count > 0)
			{
				// [DL] Splash screen done, load menu
				touchCooldown=15;
				gameScene.RemoveChild(interfaceSprite,true);	
				ResetGame();
				
				interfaceTextureInfo = new TextureInfo("/Application/textures/mainMenu.png");
				interfaceSprite = new SpriteUV(interfaceTextureInfo);
				interfaceSprite.Scale = new Vector2(Director.Instance.GL.Context.GetViewport ().Width, Director.Instance.GL.Context.GetViewport ().Height);
			
				gameScene.AddChild(interfaceSprite);
				gameState = States.MainMenu;	
			}
			else
			{
				// [DL] Else update scene objects	
				sceneObjectsUpdate();
			}
		}
		
		public static void ProcessMainMenuState()
		{
			// [DL] Convert touchdata to vector2, and check if user has tapped a menu button
			if(Touch.GetData (0).Count > 0)
			{
				if(touchCooldown<=0)
				{
					touchCooldown=15;
					Vector2 v = Input2.Touch00.Pos;
					if(v.X >=0)
					{
						v = new Vector2((v.X * 470) + 470, (v.Y * 280)+280);	
					}
					else
					{
						v = new Vector2(((v.X+1) * 470), ((v.Y+1) * 280));
					}
					
					if(v.X >= 383 && v.X <= 555 && v.Y >= 247 && v.Y <= 308)
					{
						// [DL]Tapped 'play game'
						gameScene.RemoveChild(interfaceSprite,true);					
						
						interfaceTextureInfo = new TextureInfo("/Application/textures/levelSelect.png");
						interfaceSprite = new SpriteUV(interfaceTextureInfo);
						interfaceSprite.Scale = new Vector2(Director.Instance.GL.Context.GetViewport ().Width, Director.Instance.GL.Context.GetViewport ().Height);
					
						gameScene.AddChild(interfaceSprite);
						gameState = States.LevelSelect;				
					}else if(v.X >= 383 && v.X <= 555 && v.Y >= 160 && v.Y <= 220)
					{
						// [RB] credits code
						gameScene.RemoveChild(interfaceSprite,true);					
					
						interfaceTextureInfo = new TextureInfo("/Application/textures/credits.png");
						interfaceSprite = new SpriteUV(interfaceTextureInfo);
						interfaceSprite.Scale = new Vector2(Director.Instance.GL.Context.GetViewport ().Width, Director.Instance.GL.Context.GetViewport ().Height);
				
						gameScene.AddChild(interfaceSprite);
						gameState = States.Credits;
					}
				}
			}
		}
		
		public static void ProcessLevelSelectState()
		{
			// [DL] Convert touchdata to vector2, and check if user has tapped a level button			
			if(Touch.GetData (0).Count > 0)
			{
				if(touchCooldown<=0)
				{
					touchCooldown=15;
					Vector2 v = Input2.Touch00.Pos;
					if(v.X >=0)
					{
						v = new Vector2((v.X * 470) + 470, (v.Y * 280)+280);	
					}
					else
					{
						v = new Vector2(((v.X+1) * 470), ((v.Y+1) * 280));
					}
					
					if(v.X >= 105 && v.X <= 341 && v.Y >= 114 && v.Y <= 346)
					{	
						// [DL] tapped easy
						gameScene.RemoveChild(interfaceSprite,true);
						setupEasyMap();
						gameState = States.Game;
					}
					else if(v.X >= 341 && v.X <= 594 && v.Y >= 114 && v.Y <= 346)
					{	
						// [DL] tapped medium
						gameScene.RemoveChild(interfaceSprite,true);
						setupMediumMap();
						gameState = States.Game;
					}
					else if(v.X >= 594 && v.X <= 822 && v.Y >= 114 && v.Y <= 346)
					{	
						// [DL] tapped hard
						gameScene.RemoveChild(interfaceSprite,true);
						setupHardMap();
						gameState = States.Game;
					}
					else if(v.X >= 386 && v.X <= 548 && v.Y >= 27 && v.Y <= 84)
					{	
						// [DL]tapped back to menu
						gameScene.RemoveChild(interfaceSprite,true);
						
						interfaceTextureInfo = new TextureInfo("/Application/textures/mainMenu.png");
						interfaceSprite = new SpriteUV(interfaceTextureInfo);
						interfaceSprite.Scale = new Vector2(Director.Instance.GL.Context.GetViewport ().Width, Director.Instance.GL.Context.GetViewport ().Height);
					
						gameScene.AddChild(interfaceSprite);
						gameState = States.MainMenu;
					}
				}
			}
		}
				
		public static void ProcessSettingsState()
		{
			// Add code to check if toggle settings
			
			// [DL] If game state is no longer settings, clean up
			if(gameState!= States.Settings){interfaceTextureInfo.Dispose();}
		}
		
		public static void checkWaveStatus()
		{
			// [DL] Check if any active creeps, and if not gameover, show spawnwave, else show gameover screen
			if(creeps.Count() == 0 && creepQueue.Count() == 0)
			{	
				if(wave < numberOfWaves)
				{
					// [DL] Wave isn't active, but more waves so show spawnWave button
					speed = 0;
				}
				else
				{
					// [DL] GameOver!
					ResetGame();
					gameState = States.GameOver;
					
					UISystem.SetScene(gameOverScene);
					gameOverScene.Visible = true;
					
					creepsKilledLabel.Text = "Creeps Killed: " + creepsKilled.ToString();
					creepsEscapedLabel.Text = "Creeps Escaped: " + creepsEscaped.ToString();
					finalScoreLabel.Text = "Final Score: " + score.ToString();
					
					interfaceTextureInfo = new TextureInfo("/Application/textures/gameOver.png");
					interfaceSprite = new SpriteUV(interfaceTextureInfo);
					interfaceSprite.Scale = new Vector2(Director.Instance.GL.Context.GetViewport ().Width, Director.Instance.GL.Context.GetViewport ().Height);
				
					gameScene.AddChild(interfaceSprite);
					
				}
			}
		}
		
		public static void ProcessGameState()
		{			
			checkWaveStatus();
			updateScore();
			
			// [DL] Check if screen is tapped, work out where, and apply functionality
			if(Touch.GetData (0).Count > 0)
			{
				if(touchCooldown<=0)
				{
					// [DL] Tapped on screen, start tap cooldown, convert touch position to vector coordinates
					touchCooldown=15;
					Vector2 v = Input2.Touch00.Pos;
									
					if(v.X >=0)
					{
						v = new Vector2((v.X * 470) + 470, (v.Y * 280)+280);	
					}
					else
					{
						v = new Vector2(((v.X+1) * 470), ((v.Y+1) * 280));
					}
					
					if(v.X <= 800 && v.Y <= 500)
					{
						// [DL] Tapped on map
						selectedGridCoords = new int[] {(int)System.Math.Floor(v.X/50.0f),(int)System.Math.Floor(v.Y/50.0f)};
						
						gameScene.RemoveChild(targetSprite, true);
						targetTextureInfo = new TextureInfo("/Application/textures/selected.png");
						targetSprite = new SpriteUV(targetTextureInfo);
						targetSprite.Scale = new Vector2(50.0f, 50.0f);
						targetSprite.Position = new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50);
						
						gameScene.AddChild(targetSprite);
						gameScene.RemoveChild(rangeSprite, true);
						
						// [DL] Check if a tower is in this grid spot, and apply range graphic						
						if(gridStatus[selectedGridCoords[0],selectedGridCoords[1]]>1)
						{
							BaseTower b2 = towers.Find(BaseTower => BaseTower.Position == new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
							
							// [DL] Check if a tower is in this grid spot, and apply range graphic
							if(!(b2 is EMP || b2 is Mud || b2 is BarbedWire))
							{
								// [DL] Setup range sprite
								rangeTextureInfo = new TextureInfo("/Application/textures/range.png");
								rangeSprite = new SpriteUV(rangeTextureInfo);
								
								float range2 = (float)b2.getRange*50*4;
								rangeSprite.Scale = new Vector2(range2, range2);
								rangeSprite.Position = new Vector2(selectedGridCoords[0]*50-range2/2+25, selectedGridCoords[1]*50-range2/2+25);
								
								gameScene.AddChild(rangeSprite);
							}
						}
						if(gridStatus[selectedGridCoords[0],selectedGridCoords[1]] != gameUIState)
						{
							//add game state stuff
							gameUIState = gridStatus[selectedGridCoords[0],selectedGridCoords[1]];
							updateInterface();
						}
					}
					else
					{
						// [DL] Check if tapped spawnWave / speedUp / Slow Down, set speed so relevant graphic is displayed in updateInterface
						if(v.X >= 815 && v.X <= 945 && v.Y >= 35 && v.Y <= 52)
						{
							if(speed==1)
							{
								speed=3;	
							}
							else if(speed==3)
							{
								speed=1;	
							}
							else
							{
								speed=1;
								queueNextWave();
							}
							
							updateInterface();
						}
						
						// [DL] Add, scale and position range sprite at grid location if a tower is selected
						gameScene.RemoveChild(rangeSprite, true);
						
						if(selectedGridCoords!=null)
						{
							BaseTower b2 = towers.Find(BaseTower => BaseTower.Position == new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
							
							// [DL] Check if a tower is in this grid spot, and apply range graphic
							if(!(b2 is EMP || b2 is Mud || b2 is BarbedWire || b2 == null))
							{
								// [DL] Setup range sprite
								rangeTextureInfo = new TextureInfo("/Application/textures/range.png");
								rangeSprite = new SpriteUV(rangeTextureInfo);
								
								float range2 = (float)b2.getRange*50*4;
								rangeSprite.Scale = new Vector2(range2, range2);
								rangeSprite.Position = new Vector2(selectedGridCoords[0]*50-range2/2+25, selectedGridCoords[1]*50-range2/2+25);
								
								gameScene.AddChild(rangeSprite);
							}
						}
						
						switch(gameUIState)
						{
						case(0):
							gameScene.RemoveChild(rangeSprite, true);
							
							if(v.X >= 800 && v.X <= 859 && v.Y >= 323 && v.Y <= 392)
							{
								// [DL] Open AutoCannon Stats
								gameUIState = 2;
								updateInterface();
								
								// [DL] Setup range sprite
								gameScene.RemoveChild(rangeSprite, true);
								rangeTextureInfo = new TextureInfo("/Application/textures/range.png");
								rangeSprite = new SpriteUV(rangeTextureInfo);
								rangeSprite.Scale = new Vector2(150.0f, 150.0f);
								rangeSprite.Position = new Vector2(selectedGridCoords[0]*50.0f-50, selectedGridCoords[1]*50.0f-50);
								gameScene.AddChild(rangeSprite);
							}
							else if(v.X >= 860 && v.X <= 920 && v.Y >= 323 && v.Y <= 392)
							{
								// [DL] Open HeavyArtillery Stats
								gameUIState = 4;
								updateInterface();
								
								// [DL] Setup range sprite
								gameScene.RemoveChild(rangeSprite, true);
								rangeTextureInfo = new TextureInfo("/Application/textures/range.png");
								rangeSprite = new SpriteUV(rangeTextureInfo);
								rangeSprite.Scale = new Vector2(250.0f, 250.0f);
								rangeSprite.Position = new Vector2(selectedGridCoords[0]*50.0f-100, selectedGridCoords[1]*50.0f-100);
								gameScene.AddChild(rangeSprite);
							}
							break;
						case(1):
							gameScene.RemoveChild(rangeSprite, true);
							
							if(v.X >= 823 && v.X <= 892 && v.Y >= 327 && v.Y <= 388)
							{
								// [DL] Open Mud Stats
								gameUIState = 6;
								updateInterface();
							}
							break;
						case(2):
							if(v.X >= 860 && v.X <= 920 && v.Y >= 323 && v.Y <= 392)
							{
								// [DL] Open HeavyArtillery Stats
								gameUIState = 4;
								updateInterface();
								
								// [DL] Setup range sprite
								gameScene.RemoveChild(rangeSprite, true);
								rangeTextureInfo = new TextureInfo("/Application/textures/range.png");
								rangeSprite = new SpriteUV(rangeTextureInfo);
								rangeSprite.Scale = new Vector2(250.0f, 250.0f);
								rangeSprite.Position = new Vector2(selectedGridCoords[0]*50.0f-100, selectedGridCoords[1]*50.0f-100);
								gameScene.AddChild(rangeSprite);
							}
							else if(v.X >= 800 && v.X <= 921 && v.Y >= 76 && v.Y <= 116)
							{
								if(withdrawGold(2))
								{
									// [DL] Create an AutoCannon, add it to list. Clear the UI and remove grid target sprite
									AutoCannon ac = new AutoCannon(gameScene, new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
									towers.Add(ac);
									
									gameUIState = -1;
									updateInterface ();
									gridStatus[selectedGridCoords[0],selectedGridCoords[1]] = 3;
									gameScene.RemoveChild(targetSprite, true);
									gameScene.RemoveChild(rangeSprite, true);
								}
							}
							break;
						case(3):
							if(v.X >= 800 && v.X <= 859 && v.Y >= 323 && v.Y <= 392)
							{
								// [DL] Open ChainGun Stats
								gameUIState = 14;
								updateInterface ();
								
								// [DL] Setup range sprite
								gameScene.RemoveChild(rangeSprite, true);
								rangeTextureInfo = new TextureInfo("/Application/textures/range.png");
								rangeSprite = new SpriteUV(rangeTextureInfo);
								rangeSprite.Scale = new Vector2(375.0f, 375.0f);
								rangeSprite.Position = new Vector2(selectedGridCoords[0]*50.0f-162.5f, selectedGridCoords[1]*50.0f-162.5f);
								gameScene.AddChild(rangeSprite);
							}
							else if(v.X >= 860 && v.X <= 920 && v.Y >= 323 && v.Y <= 392)
							{
								// [DL] Open Sniper Stats
								gameUIState = 12;
								updateInterface ();
								
								// [DL] Setup range sprite
								gameScene.RemoveChild(rangeSprite, true);
								rangeTextureInfo = new TextureInfo("/Application/textures/range.png");
								rangeSprite = new SpriteUV(rangeTextureInfo);
								rangeSprite.Scale = new Vector2(600.0f, 600.0f);
								rangeSprite.Position = new Vector2(selectedGridCoords[0]*50.0f-275, selectedGridCoords[1]*50.0f-275);
								gameScene.AddChild(rangeSprite);
							}
							else if(v.X >= 800 && v.X <= 921 && v.Y >= 53 && v.Y <= 100)
							{
								// [DL] Find tower at this location, invoke its sell method, clear its gridStatus and uiState
								BaseTower b = towers.Find(BaseTower => BaseTower.Position == new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
								b.sell (gameScene);
								towers.Remove(b);
								gridStatus[selectedGridCoords[0],selectedGridCoords[1]] = 0;
								gameUIState = -1;
								updateInterface();
								gameScene.RemoveChild(rangeSprite,true);
								gameScene.RemoveChild(targetSprite,true);
							}
							break;
						case(4):							
							if(v.X >= 800 && v.X <= 859 && v.Y >= 323 && v.Y <= 392)
							{
								// [DL] Open AutoCannon Stats
								gameUIState = 2;
								updateInterface ();
								
								// [DL] Setup range sprite
								gameScene.RemoveChild(rangeSprite, true);
								rangeTextureInfo = new TextureInfo("/Application/textures/range.png");
								rangeSprite = new SpriteUV(rangeTextureInfo);
								rangeSprite.Scale = new Vector2(150.0f, 150.0f);
								rangeSprite.Position = new Vector2(selectedGridCoords[0]*50.0f-50.0f, selectedGridCoords[1]*50.0f-50.0f);
								gameScene.AddChild(rangeSprite);
							}
							else if(v.X >= 800 && v.X <= 921 && v.Y >= 76 && v.Y <= 116)
							{
								if(withdrawGold(10))
								{
									// [DL] Create a HeavyArtillery, add it to list. Clear the UI and remove grid target sprite
									HeavyArtillery ha = new HeavyArtillery(gameScene, new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
									towers.Add(ha);
									gameUIState = -1;
									updateInterface ();
									gridStatus[selectedGridCoords[0],selectedGridCoords[1]] = 5;	
									gameScene.RemoveChild(targetSprite, true);
									gameScene.RemoveChild(rangeSprite, true);
								}
							}
							break;
						case(5):
							gameScene.RemoveChild(rangeSprite, true);
							
							if(v.X >= 800 && v.X <= 859 && v.Y >= 323 && v.Y <= 392)
							{
								// [DL] Open Rocket Launcher Stats
								gameUIState = 16;
								updateInterface();
								
								// [DL] Setup range sprite
								gameScene.RemoveChild(rangeSprite, true);
								rangeTextureInfo = new TextureInfo("/Application/textures/range.png");
								rangeSprite = new SpriteUV(rangeTextureInfo);
								rangeSprite.Scale = new Vector2(300.0f, 300.0f);
								rangeSprite.Position = new Vector2(selectedGridCoords[0]*50.0f-125, selectedGridCoords[1]*50.0f-125);
								gameScene.AddChild(rangeSprite);
							}
							else if(v.X >= 860 && v.X <= 920 && v.Y >= 323 && v.Y <= 392)
							{
								// [DL] Open Missile Launcher Stats
								gameUIState = 18;
								updateInterface();
								
								// [DL] Setup range sprite
								gameScene.RemoveChild(rangeSprite, true);
								rangeTextureInfo = new TextureInfo("/Application/textures/range.png");
								rangeSprite = new SpriteUV(rangeTextureInfo);
								rangeSprite.Scale = new Vector2(450.0f, 450.0f);
								rangeSprite.Position = new Vector2(selectedGridCoords[0]*50.0f-250, selectedGridCoords[1]*50.0f-250);
								gameScene.AddChild(rangeSprite);
							}
							
							if(v.X >= 800 && v.X <= 921 && v.Y >= 53 && v.Y <= 110)
							{
								// [DL] Find tower at this location, invoke its sell method, clear its gridStatus and uiState
								BaseTower b = towers.Find(BaseTower => BaseTower.Position == new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
								b.sell (gameScene);
								towers.Remove(b);
								gridStatus[selectedGridCoords[0],selectedGridCoords[1]] = 0;
								gameUIState = -1;
								updateInterface();
								gameScene.RemoveChild(rangeSprite,true);
								gameScene.RemoveChild(targetSprite,true);
							}
							break;
						case(6):
							if(v.X >= 800 && v.X <= 921 && v.Y >= 76 && v.Y <= 116)
							{
								if(withdrawGold(1))
								{
									// [DL] Create Mud, add it to list. Clear the UI and remove grid target sprite
									Mud m = new Mud(gameScene, new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
									towers.Add(m);
									gameUIState = -1;
									updateInterface ();
									gridStatus[selectedGridCoords[0],selectedGridCoords[1]] = 7;
									gameScene.RemoveChild(targetSprite, true);
								}
							}
							break;
						case(7):
							if(v.X >= 800 && v.X <= 859 && v.Y >= 323 && v.Y <= 392)
							{
								// [DL] Open Barbed Wire Stats
								gameUIState = 8;
								updateInterface ();
							}else if(v.X >= 860 && v.X <= 920 && v.Y >= 323 && v.Y <= 392)
							{
								// [DL] Open Electric Fence Stats
								gameUIState = 10;
								updateInterface ();
							}else if(v.X >= 800 && v.X <= 921 && v.Y >= 53 && v.Y <= 110)
							{
								// [DL] Find tower at this location, invoke its sell method, clear its gridStatus and uiState
								BaseTower b = towers.Find(BaseTower => BaseTower.Position == new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
								b.sell (gameScene);
								towers.Remove(b);
								gridStatus[selectedGridCoords[0],selectedGridCoords[1]] = 1;
								gameUIState = -1;
								updateInterface();
								gameScene.RemoveChild(targetSprite,true);
								gameScene.RemoveChild(rangeSprite,true);
							}
							break;
						case(8):
							if(v.X >= 860 && v.X <= 920 && v.Y >= 323 && v.Y <= 392)
							{
								// [DL] Open Electric Fence Stats
								gameUIState = 10;
								updateInterface();	;
							}
							else if(v.X >= 800 && v.X <= 921 && v.Y >= 76 && v.Y <= 116)
							{
								if(withdrawGold(3))
								{
									// [DL] Sell current tower at this location
									BaseTower b = towers.Find(BaseTower => BaseTower.Position == new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
									b.upgrade (gameScene);
									towers.Remove(b);
									gameScene.RemoveChild(rangeSprite,true);
									gameScene.RemoveChild(targetSprite,true);
									
									// [DL] Create BarbedWire, add it to list. Clear the UI and remove grid target sprite
									BarbedWire bw = new BarbedWire(gameScene, new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
									towers.Add(bw);
									gameUIState = -1;
									updateInterface ();
									gridStatus[selectedGridCoords[0],selectedGridCoords[1]] = 9;	
									gameScene.RemoveChild(targetSprite, true);
								}
							}
							break;
						case(9):
							if(v.X >= 800 && v.X <= 921 && v.Y >= 53 && v.Y <= 110)
							{
								// [DL] Find tower at this location, invoke its sell method, clear its gridStatus and uiState
								BaseTower b = towers.Find(BaseTower => BaseTower.Position == new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
								b.sell (gameScene);
								towers.Remove(b);
								gameScene.RemoveChild(targetSprite,true);
								gameScene.RemoveChild(rangeSprite,true);
								gridStatus[selectedGridCoords[0],selectedGridCoords[1]] = 1;
								gameUIState = -1;
								updateInterface();
							}
							break;
						case(10):
							if(v.X >= 800 && v.X <= 859 && v.Y >= 323 && v.Y <= 392)
							{
								// [DL] Open BarbedWire Stats
								gameUIState = 8;
								updateInterface ();
							}
							else if(v.X >= 800 && v.X <= 921 && v.Y >= 76 && v.Y <= 116)
							{
								if(withdrawGold(15))
								{
									// [DL] Sell current tower at this location
									BaseTower b = towers.Find(BaseTower => BaseTower.Position == new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
									b.upgrade (gameScene);
									towers.Remove(b);
									gameScene.RemoveChild(targetSprite,true);
									gameScene.RemoveChild(rangeSprite,true);
									
									// [DL] Create an EMP, add it to list. Clear the UI and remove grid target sprite
									EMP e = new EMP(gameScene, new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
									towers.Add(e);
									gameUIState = -1;
									updateInterface ();
									gridStatus[selectedGridCoords[0],selectedGridCoords[1]] = 11;
									gameScene.RemoveChild(targetSprite, true);
								}
							}
							break;
						case(11):
							if(v.X >= 800 && v.X <= 921 && v.Y >= 53 && v.Y <= 110)
							{
								// [DL] Find tower at this location, invoke its sell method, clear its gridStatus and uiState
								BaseTower b = towers.Find(BaseTower => BaseTower.Position == new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
								b.sell (gameScene);
								towers.Remove(b);
								gameScene.RemoveChild(rangeSprite,true);
								gameScene.RemoveChild(targetSprite,true);
								gridStatus[selectedGridCoords[0],selectedGridCoords[1]] = 1;
								gameUIState = -1;
								updateInterface();
							}
							break;
						case(12):
							if(v.X >= 800 && v.X <= 859 && v.Y >= 323 && v.Y <= 392)
							{
								// [DL] Open Chaingun Stats
								gameUIState = 14;
								updateInterface ();		
								
								// [DL] Setup range sprite
								gameScene.RemoveChild(rangeSprite, true);
								rangeTextureInfo = new TextureInfo("/Application/textures/range.png");
								rangeSprite = new SpriteUV(rangeTextureInfo);
								rangeSprite.Scale = new Vector2(375.0f, 375.0f);
								rangeSprite.Position = new Vector2(selectedGridCoords[0]*50.0f-162.5f, selectedGridCoords[1]*50.0f-162.5f);
								gameScene.AddChild(rangeSprite);	
							}
							else if(v.X >= 800 && v.X <= 921 && v.Y >= 76 && v.Y <= 116)
							{
								if(withdrawGold(10))
								{
									// [DL] Sell current tower at this location
									BaseTower b = towers.Find(BaseTower => BaseTower.Position == new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
									b.upgrade (gameScene);
									towers.Remove(b);
									gameScene.RemoveChild(targetSprite,true);
									gameScene.RemoveChild(rangeSprite,true);
									
									// [DL] Create an AutoCannon, add it to list. Clear the UI and remove grid target sprite
									Sniper s = new Sniper(gameScene, new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
									towers.Add(s);
									gameUIState = -1;
									updateInterface ();
									gridStatus[selectedGridCoords[0],selectedGridCoords[1]] = 13;
									gameScene.RemoveChild(targetSprite, true);
									gameScene.RemoveChild(rangeSprite, true);
								}
							}
							break;
						case(13):
							if(v.X >= 800 && v.X <= 921 && v.Y >= 53 && v.Y <= 110)
							{
								// [DL] Find tower at this location, invoke its sell method, clear its gridStatus and uiState
								BaseTower b = towers.Find(BaseTower => BaseTower.Position == new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
								b.sell (gameScene);
								towers.Remove(b);
								gameScene.RemoveChild(targetSprite,true);
								gameScene.RemoveChild(rangeSprite,true);
								gridStatus[selectedGridCoords[0],selectedGridCoords[1]] = 0;
								gameUIState = -1;
								updateInterface();
							}
							break;
						case(14):
							if(v.X >= 860 && v.X <= 920 && v.Y >= 323 && v.Y <= 392)
							{
								// [DL] Open Sniper Stats
								gameUIState = 12;
								updateInterface();	
								
								// [DL] Setup range sprite
								gameScene.RemoveChild(rangeSprite, true);
								rangeTextureInfo = new TextureInfo("/Application/textures/range.png");
								rangeSprite = new SpriteUV(rangeTextureInfo);
								rangeSprite.Scale = new Vector2(600.0f, 600.0f);
								rangeSprite.Position = new Vector2(selectedGridCoords[0]*50.0f-275, selectedGridCoords[1]*50.0f-275);
								gameScene.AddChild(rangeSprite);		
							}
							else if(v.X >= 800 && v.X <= 921 && v.Y >= 76 && v.Y <= 116)
							{
								if(withdrawGold(6))
								{
									// [DL] Sell current tower at this location
									BaseTower b = towers.Find(BaseTower => BaseTower.Position == new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
									b.upgrade (gameScene);
									towers.Remove(b);
									gameScene.RemoveChild(targetSprite,true);
									gameScene.RemoveChild(rangeSprite,true);
								
									// [DL] Create a Chaingun, add it to list. Clear the UI and remove grid target sprite
									Chaingun cg = new Chaingun(gameScene, new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
									towers.Add(cg);
									gameUIState = -1;
									updateInterface ();
									gridStatus[selectedGridCoords[0],selectedGridCoords[1]] = 15;
									gameScene.RemoveChild(targetSprite, true);
									gameScene.RemoveChild(rangeSprite, true);
								}
							}
							break;
						case(15):
							if(v.X >= 800 && v.X <= 921 && v.Y >= 53 && v.Y <= 110)
							{
								// [DL] Find tower at this location, invoke its sell method, clear its gridStatus and uiState
								BaseTower b = towers.Find(BaseTower => BaseTower.Position == new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
								b.sell (gameScene);
								towers.Remove(b);
								gameScene.RemoveChild(targetSprite,true);
								gameScene.RemoveChild(rangeSprite,true);
								gridStatus[selectedGridCoords[0],selectedGridCoords[1]] = 0;
								gameUIState = -1;
								updateInterface();
							}
							break;
						case(16):
							if(v.X >= 860 && v.X <= 920 && v.Y >= 323 && v.Y <= 392)
							{
								// [DL] Open Missile Launcher Stats
								gameUIState = 18;
								updateInterface();
								
								// [DL] Setup range sprite
								gameScene.RemoveChild(rangeSprite, true);
								rangeTextureInfo = new TextureInfo("/Application/textures/range.png");
								rangeSprite = new SpriteUV(rangeTextureInfo);
								rangeSprite.Scale = new Vector2(450.0f, 450.0f);
								rangeSprite.Position = new Vector2(selectedGridCoords[0]*50.0f-250, selectedGridCoords[1]*50.0f-250);
								gameScene.AddChild(rangeSprite);
							}
							else if(v.X >= 800 && v.X <= 921 && v.Y >= 76 && v.Y <= 116)
							{
								if(withdrawGold(8))
								{
									// [DL] Sell current tower at this location
									BaseTower b = towers.Find(BaseTower => BaseTower.Position == new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
									b.upgrade (gameScene);
									towers.Remove(b);
									gameScene.RemoveChild(targetSprite,true);
									gameScene.RemoveChild(rangeSprite,true);
									
									// [DL] Create a RocketLauncher, add it to list. Clear the UI and remove grid target sprite
									RocketLauncher rl = new RocketLauncher(gameScene, new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
									towers.Add(rl);
									gameUIState = -1;
									updateInterface ();
									gridStatus[selectedGridCoords[0],selectedGridCoords[1]] = 17;
									gameScene.RemoveChild(targetSprite, true);
									gameScene.RemoveChild(rangeSprite, true);
								}
							}
							break;
						case(17):
							if(v.X >= 800 && v.X <= 921 && v.Y >= 53 && v.Y <= 110)
							{
								// [DL] Find tower at this location, invoke its sell method, clear its gridStatus and uiState
								BaseTower b = towers.Find(BaseTower => BaseTower.Position == new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
								b.sell (gameScene);
								towers.Remove(b);
								gameScene.RemoveChild(targetSprite,true);
								gameScene.RemoveChild(rangeSprite,true);
								gridStatus[selectedGridCoords[0],selectedGridCoords[1]] = 0;
								gameUIState = -1;
								updateInterface();
							}
							break;
						case(18):
							if(v.X >= 800 && v.X <= 859 && v.Y >= 323 && v.Y <= 392)
							{
								// [DL] Open RocketLauncher Stats
								gameUIState = 16;
								updateInterface ();
								
								// [DL] Setup range sprite
								gameScene.RemoveChild(rangeSprite, true);
								rangeTextureInfo = new TextureInfo("/Application/textures/range.png");
								rangeSprite = new SpriteUV(rangeTextureInfo);
								rangeSprite.Scale = new Vector2(300.0f, 300.0f);
								rangeSprite.Position = new Vector2(selectedGridCoords[0]*50.0f-125, selectedGridCoords[1]*50.0f-125);
								gameScene.AddChild(rangeSprite);
							}
							else if(v.X >= 800 && v.X <= 921 && v.Y >= 76 && v.Y <= 116)
							{
								if(withdrawGold(6))
								{
									// [DL] Sell current tower at this location
									BaseTower b = towers.Find(BaseTower => BaseTower.Position == new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
									b.upgrade (gameScene);
									towers.Remove(b);
									gameScene.RemoveChild(targetSprite,true);
									gameScene.RemoveChild(rangeSprite,true);
								
									// [DL] Create a Missile Launcher, add it to list. Clear the UI and remove grid target sprite
									MissileLauncher ml = new MissileLauncher(gameScene, new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
									towers.Add(ml);
									gameUIState = -1;
									updateInterface ();
									gridStatus[selectedGridCoords[0],selectedGridCoords[1]] = 19;
									gameScene.RemoveChild(targetSprite, true);
									gameScene.RemoveChild(rangeSprite, true);
								}
							}
							break;
						case(19):
							if(v.X >= 800 && v.X <= 921 && v.Y >= 53 && v.Y <= 110)
							{
								// [DL] Find tower at this location, invoke its sell method, clear its gridStatus and uiState
								BaseTower b = towers.Find(BaseTower => BaseTower.Position == new Vector2(selectedGridCoords[0]*50, selectedGridCoords[1]*50));
								b.sell (gameScene);
								towers.Remove(b);
								gameScene.RemoveChild(rangeSprite,true);
								gameScene.RemoveChild(targetSprite,true);
								gridStatus[selectedGridCoords[0],selectedGridCoords[1]] = 0;
								gameUIState = -1;
								updateInterface();
							}
							break;
						}
					}
				}
			}
		}
				
		public static void ProcessGameOverState()
		{
			//[RB] return to main menu
			if(Touch.GetData (0).Count > 0)
			{
				if(touchCooldown<=0)
				{
					touchCooldown=15;
					Vector2 v = Input2.Touch00.Pos;
					
					if(v.X >=0)
					{
						v = new Vector2((v.X * 470) + 470, (v.Y * 280)+280);	
					}
					else
					{
						v = new Vector2(((v.X+1) * 470), ((v.Y+1) * 280));
					}
				
					if(v.X >= 386 && v.X <= 548 && v.Y >= 27 && v.Y <= 84)
					{	
						// [DL]tapped back to menu
						gameScene.RemoveChild(interfaceSprite,true);
						
						interfaceTextureInfo = new TextureInfo("/Application/textures/mainMenu.png");
						interfaceSprite = new SpriteUV(interfaceTextureInfo);
						interfaceSprite.Scale = new Vector2(Director.Instance.GL.Context.GetViewport ().Width, Director.Instance.GL.Context.GetViewport ().Height);
					
						gameScene.AddChild(interfaceSprite);
						gameState = States.MainMenu;
						
						UISystem.SetScene(uiScene);
					}
				}
			}
		}
		
		public static void ProcessTutorialState()
		{
			// Add gameplay code with scripted tutorial	
			gameState = States.Game;
		}
		
		public static void ProcessHighScoreState()
		{
			// [RB] add high score display code
			if(Touch.GetData (0).Count > 0)
			{
				if(touchCooldown<=0)
				{	
					touchCooldown=15;
					Vector2 v = Input2.Touch00.Pos;
					
					if(v.X >=0)
					{
						v = new Vector2((v.X * 470) + 470, (v.Y * 280)+280);	
					}
					else
					{
						v = new Vector2(((v.X+1) * 470), ((v.Y+1) * 280));
					}
					
					if(v.X >= 20 && v.X <= 285 && v.Y >= 20 && v.Y <= 75)
					{
						// [RB] code to clear high scores here
					}
					else if(v.X >= 655 && v.X <= 920 && v.Y >= 20 && v.Y <= 75)
					{	
						// [RB] return to menu
						gameScene.RemoveChild(interfaceSprite,true);
						
						interfaceTextureInfo = new TextureInfo("/Application/textures/mainMenu.png");
						interfaceSprite = new SpriteUV(interfaceTextureInfo);
						interfaceSprite.Scale = new Vector2(Director.Instance.GL.Context.GetViewport ().Width, Director.Instance.GL.Context.GetViewport ().Height);
					
						gameScene.AddChild(interfaceSprite);
						gameState = States.MainMenu;
					}
				}
			}
		}
		
		public static void ProcessCreditsState()
		{	
			//[RB] return to main menu
			if(Touch.GetData (0).Count > 0)
			{
				if(touchCooldown<=0)
				{
					touchCooldown=15;
					Vector2 v = Input2.Touch00.Pos;
					
					if(v.X >=0)
					{
						v = new Vector2((v.X * 470) + 470, (v.Y * 280)+280);	
					}
					else
					{
						v = new Vector2(((v.X+1) * 470), ((v.Y+1) * 280));
					}
				
					if(v.X >= 386 && v.X <= 548 && v.Y >= 27 && v.Y <= 84)
					{	
						// [RB]tapped back to menu
						gameScene.RemoveChild(interfaceSprite,true);
						
						interfaceTextureInfo = new TextureInfo("/Application/textures/mainMenu.png");
						interfaceSprite = new SpriteUV(interfaceTextureInfo);
						interfaceSprite.Scale = new Vector2(Director.Instance.GL.Context.GetViewport ().Width, Director.Instance.GL.Context.GetViewport ().Height);
					
						gameScene.AddChild(interfaceSprite);
						gameState = States.MainMenu;
					}
				}
			}
		}
		
		public static void setupTutorialMap()
		{
			gridStatus = tutorialGrid;
			mapLevel = 0;
			score = 0.0f;
			gold = 50.0f;
			creepsEscaped = 0;
			creepsKilled = 0;
			numberOfWaves = 5;
						
			uiScene.Visible = true;
			
			tutorialWaypoints = new Vector2[easyWPx.Length];
			for(int i = 0; i<easyWPx.Length; i++)
			{
				Vector2 v = new Vector2(easyWPx[i],easyWPy[i]);
				tutorialWaypoints[i]=v;	
			}
			
			mapTextureInfo = new TextureInfo("/Application/textures/Maps/tutorialMap.png");
			mapSprite = new SpriteUV(mapTextureInfo); 	
			mapSprite.Scale = new Vector2(800.0f,500.0f);
			
			interfaceTextureInfo = new TextureInfo("/Application/textures/Interface/" + gameUIState.ToString() + ".png");
			interfaceSprite = new SpriteUV(interfaceTextureInfo); 	
			interfaceSprite.Scale = new Vector2(Director.Instance.GL.Context.GetViewport ().Width, Director.Instance.GL.Context.GetViewport ().Height);
			
			startWaveTextureInfo = new TextureInfo("/Application/textures/startWave.png");
			startWaveSprite = new SpriteUV(startWaveTextureInfo); 
			startWaveSprite.Position = new Vector2(819f,24f);
			startWaveSprite.Scale = new Vector2(123f,39f);
					
			gameScene.AddChild(interfaceSprite);
			gameScene.AddChild(startWaveSprite);	
			gameScene.AddChild(mapSprite);	
		}
		
		public static void setupEasyMap()
		{
			gridStatus = easyGrid;
			mapLevel = 1;
			score = 0.0f;
			gold = 65.0f;
			creepsEscaped = 0;
			creepsKilled = 0;
			numberOfWaves = 5;
			
			uiScene.Visible = true;
			
			easyWaypoints = new Vector2[easyWPx.Length];
			for(int i = 0; i<easyWPx.Length; i++)
			{
				Vector2 v = new Vector2(easyWPx[i],easyWPy[i]);
				easyWaypoints[i]=v;	
			}
			
			mapTextureInfo = new TextureInfo("/Application/textures/Maps/easyMap.png");
			mapSprite = new SpriteUV(mapTextureInfo); 	
			mapSprite.Scale = new Vector2(800.0f,500.0f);
			
			interfaceTextureInfo = new TextureInfo("/Application/textures/Interface/" + gameUIState.ToString() + ".png");
			interfaceSprite = new SpriteUV(interfaceTextureInfo); 	
			interfaceSprite.Scale = new Vector2(Director.Instance.GL.Context.GetViewport ().Width, Director.Instance.GL.Context.GetViewport ().Height);
			
			startWaveTextureInfo = new TextureInfo("/Application/textures/startWave.png");
			startWaveSprite = new SpriteUV(startWaveTextureInfo); 
			startWaveSprite.Position = new Vector2(819f,24f);
			startWaveSprite.Scale = new Vector2(123f,39f);
			
			gameScene.AddChild(interfaceSprite);
			gameScene.AddChild(startWaveSprite);	
			gameScene.AddChild(mapSprite);	
		}
		
		public static void setupMediumMap()
		{
			gridStatus = mediumGrid;
			mapLevel = 2;
			score = 0.0f;
			gold = 50.0f;
			creepsEscaped = 0;
			creepsKilled = 0;
			numberOfWaves = 5;
			
			uiScene.Visible = true;
			
			mediumWaypoints1 = new Vector2[mediumWP1x.Length];
			for(int i = 0; i<mediumWP1x.Length; i++)
			{
				Vector2 v = new Vector2(mediumWP1x[i],mediumWP1y[i]);
				mediumWaypoints1[i]=v;	
			}
			
			mediumWaypoints2 = new Vector2[mediumWP2x.Length];
			for(int i = 0; i<mediumWP2x.Length; i++)
			{
				Vector2 v = new Vector2(mediumWP2x[i],mediumWP2y[i]);
				mediumWaypoints2[i]=v;	
			}
			
			mapTextureInfo = new TextureInfo("/Application/textures/Maps/mediumMap.png");
			mapSprite = new SpriteUV(mapTextureInfo); 	
			mapSprite.Scale = new Vector2(800.0f,500.0f);
			
			interfaceTextureInfo = new TextureInfo("/Application/textures/Interface/" + gameUIState.ToString() + ".png");
			interfaceSprite = new SpriteUV(interfaceTextureInfo); 	
			interfaceSprite.Scale = new Vector2(Director.Instance.GL.Context.GetViewport ().Width, Director.Instance.GL.Context.GetViewport ().Height);
			
			startWaveTextureInfo = new TextureInfo("/Application/textures/startWave.png");
			startWaveSprite = new SpriteUV(startWaveTextureInfo); 
			startWaveSprite.Position = new Vector2(819f,24f);
			startWaveSprite.Scale = new Vector2(123f,39f);
				
			gameScene.AddChild(interfaceSprite);
			gameScene.AddChild(startWaveSprite);	
			gameScene.AddChild(mapSprite);
		}
		
		public static void setupHardMap()
		{
			gridStatus = hardGrid;
			mapLevel = 3;
			score = 0.0f;
			gold = 35.0f;
			creepsEscaped = 0;
			creepsKilled = 0;
			numberOfWaves = 5;
			
			uiScene.Visible = true;
			
			mapTextureInfo = new TextureInfo("/Application/textures/Maps/hardMap.png");
			mapSprite = new SpriteUV(mapTextureInfo); 	
			mapSprite.Scale = new Vector2(800.0f,500.0f);
			
			interfaceTextureInfo = new TextureInfo("/Application/textures/Interface/-1.png");
			interfaceSprite = new SpriteUV(interfaceTextureInfo); 	
			interfaceSprite.Scale = new Vector2(Director.Instance.GL.Context.GetViewport ().Width, Director.Instance.GL.Context.GetViewport ().Height);
			
			startWaveTextureInfo = new TextureInfo("/Application/textures/startWave.png");
			startWaveSprite = new SpriteUV(startWaveTextureInfo); 
			startWaveSprite.Position = new Vector2(819f,24f);
			startWaveSprite.Scale = new Vector2(123f,39f);
			
			gameScene.AddChild(interfaceSprite);
			gameScene.AddChild(startWaveSprite);	
			gameScene.AddChild(mapSprite);			
		}
	}
}