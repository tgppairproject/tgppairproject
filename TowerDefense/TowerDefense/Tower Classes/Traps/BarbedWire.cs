using System;

using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

using Sce.PlayStation.HighLevel.GameEngine2D;
using Sce.PlayStation.HighLevel.GameEngine2D.Base;


namespace TowerDefense
{
	public class BarbedWire : BaseTower
	{
		private static towerStats originalStats = new towerStats("textures/Towers/Traps/barbedWire.png","Barbed Wire","Slows and damages creeps",1,0.5f,0.0f,15,3);
		
		public BarbedWire (Scene scene, Vector2 v) : base(scene, originalStats,v)
		{
			
		}
	}
}

