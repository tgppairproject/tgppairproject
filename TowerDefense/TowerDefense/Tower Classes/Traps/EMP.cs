using System;

using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

using Sce.PlayStation.HighLevel.GameEngine2D;
using Sce.PlayStation.HighLevel.GameEngine2D.Base;


namespace TowerDefense
{
	public class EMP : BaseTower
	{
		private static towerStats originalStats = new towerStats("textures/Towers/Traps/emp.png","EMP","Does something shiny",1,0.5f,2.0f,15,15);
		
		public EMP (Scene scene, Vector2 v) : base(scene, originalStats,v)
		{
			
		}
	}
}

