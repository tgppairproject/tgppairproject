using System;

using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

using Sce.PlayStation.HighLevel.GameEngine2D;
using Sce.PlayStation.HighLevel.GameEngine2D.Base;


namespace TowerDefense
{
	public class Mud : BaseTower
	{
		private static towerStats originalStats = new towerStats("textures/Towers/Traps/mud.png","Mud","Slows creeps",0,0.5f,1.0f,15,1);
		
		public Mud (Scene scene, Vector2 v) : base(scene, originalStats,v)
		{
			
		}
	}
}

