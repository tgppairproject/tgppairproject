using System;
using System.Collections.Generic;

using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

using Sce.PlayStation.HighLevel.GameEngine2D;
using Sce.PlayStation.HighLevel.GameEngine2D.Base;

using System.Diagnostics;

namespace TowerDefense
{
	public class BaseTower
	{
		// [DL] Tower Stats
		protected towerStats stats = new towerStats();
		protected SpriteUV sprite;
		protected TextureInfo textureInfo;
		private float width;
		private float height;
		private Vector2 pos;
		
		public Vector2 Position { get { return pos; }}
		public float getWidth { get { return width; }}
		public float getHeight { get { return height; }}
		public float getRange { get { return stats.range; }}
		
		protected float rotation;
		
		// [DL] Target tracking
		protected BaseCreep targetCreep {get; set; }
		protected List<BaseCreep> targettedByCreep = new List<BaseCreep>(); // [DL] Whos targetting this tower, for cleanup purposes
		
		// [DL] Timers
		protected int fireCooldown; // [DL] Checks if ready to fire
		protected int targetCheck = 15; // [DL] Checks for new target every 250milliseconds
		protected int freezeCheck = 60; // [DL] When frozen, wait 2seconds to unfreeze
		
		protected SpriteUV turretSprite;
		protected TextureInfo turretTexture;
		
		public BaseTower (Scene scene, towerStats stats, Vector2 pos, String filePath)
		{
			this.stats = stats;
			fireCooldown = stats.rateOfFire;
			
			textureInfo = new TextureInfo("/Application/" + stats.graphicPath);
			sprite = new SpriteUV(textureInfo);
			sprite.Position = pos;
			this.pos = pos;
			
			turretTexture = new TextureInfo("/Application/textures/Towers/" + filePath + ".png");
			turretSprite = new SpriteUV(turretTexture);
			turretSprite.Position = new Vector2(pos.X+25,pos.Y+25);
			turretSprite.CenterSprite();
			turretSprite.Scale = new Vector2(50f,50f);
			
			sprite.Quad.S = textureInfo.TextureSizef;
			Bounds2 b = sprite.Quad.Bounds2();
			width = b.Point10.X;
			height = b.Point01.Y;
			
			scene.AddChild (sprite);
			scene.AddChild(turretSprite);
		}
		
		public BaseTower (Scene scene, towerStats stats, Vector2 pos)
		{
			this.stats = stats;
			fireCooldown = stats.rateOfFire;
			
			textureInfo = new TextureInfo("/Application/" + stats.graphicPath);
			sprite = new SpriteUV(textureInfo);
			sprite.Position = pos;
			this.pos = pos;
			rotation = 90.0f;
			
			sprite.Quad.S = textureInfo.TextureSizef;
			Bounds2 b = sprite.Quad.Bounds2();
			width = b.Point10.X;
			height = b.Point01.Y;
			
			scene.AddChild (sprite);
		}
		
		protected virtual void checkTarget(List<BaseCreep> creeps)
		{
			// [DL] Check if target is null, or out of range, then attempt to choose new target
			if(targetCreep==null)
			{
				obtainTarget (creeps);
			}
			else
			{
				// [DL] Find this and target central x positions for range checking
				float xPos = sprite.Position.X + width/2;
				float targetXPos = targetCreep.Position.X + targetCreep.getWidth/2;
				float yPos = sprite.Position.Y + height/2;
				float targetYPos = targetCreep.Position.Y + targetCreep.getHeight/2;
				
				float distance = (float)(System.Math.Sqrt((xPos - targetXPos) * (xPos - targetXPos) + (yPos - targetYPos) * (yPos - targetYPos))/50);
				if(distance > (stats.range*2.0f))
				{
					// [DL] Creep is out of range, set target to null and call obtainTarget method
					targetCreep.removeTargettedBy(this);
					targetCreep=null;
					obtainTarget (creeps);
				}
			}
			targetCheck = 60; // [DL] Reset timer
		}
		
		public void mainUpdate(Scene scene, List<BaseCreep> creeps)
		{
			if(targetCheck<=0)
			{
				targetCheck=60;
				checkTarget (creeps);	
			}
			else
			{
				targetCheck--;	
			}
			
			if(targetCreep!=null)
			{
				// [DL] Has target, fire
				fire (scene, creeps);
			}
			
			updateRotation ();
		}
		
		protected void updateRotation()
		{
			if(turretTexture!=null && targetCreep!=null)
			{
				// [DL] This is a turret as a texture was loaded, and thereforenot a trap, so rotate it
				turretSprite.Angle = calculateRotation();
			}
		}
		
		private float calculateRotation()
		{
			double angle = System.Math.Atan2((sprite.Position.Y-targetCreep.Position.Y),(sprite.Position.X-targetCreep.Position.X));
    		return (float)angle;
		}
		
		protected virtual void obtainTarget(List<BaseCreep> creeps)
		{
			if(creeps.Count > 0)
			{
				float dist = 999.0f;
				BaseCreep tempCreep = null;
				foreach(BaseCreep c in creeps)
				{
					float xPos = sprite.Position.X + width/2;
					float targetXPos = c.Position.X + c.getWidth/2;
					float yPos = sprite.Position.Y + height/2;
					float targetYPos = c.Position.Y + c.getHeight/2;
					
					float distance = (float)(System.Math.Sqrt((xPos - targetXPos) * (xPos - targetXPos) + (yPos - targetYPos) * (yPos - targetYPos))/50);
					
					if(distance<dist)
					{
						dist = distance;
						tempCreep = c;
					}
				}
				
				if(dist<(stats.range*2.0f))
				{
					targetCreep = tempCreep;
					targetCreep.addTargettedBy(this);
				}
			}
		}
		
		public void fire(Scene scene, List<BaseCreep> creeps)
		{
			if(fireCooldown<=0)
			{
				fireCooldown=stats.rateOfFire;
			
				if(this is AutoCannon || this is Sniper || this is Chaingun)
				{
					BulletProjectile bullet = new BulletProjectile(scene, sprite.Position, targetCreep, stats.damage, "bullet");
					AppMain.addProjectile(bullet);
				}
				else if(this is HeavyArtillery)
				{
					ArtilleryProjectile shell = new ArtilleryProjectile(scene, sprite.Position, targetCreep, stats.damage, "artilleryShell");
					AppMain.addProjectile(shell);
				}
				else if(this is MissileLauncher)
				{
					MissileProjectile missile = new MissileProjectile(scene, sprite.Position, targetCreep, stats.damage, "missile");
					AppMain.addProjectile(missile);
				}
				else if(this is RocketLauncher)
				{
					RocketProjectile rocket = new RocketProjectile(scene, sprite.Position, targetCreep, stats.damage, "rocket");
					AppMain.addProjectile(rocket);
				}
				else if(this is Mud || this is EMP || this is BarbedWire)
				{
					// [DL] Trap firing, loop through and hit all enemies in range
					foreach(BaseCreep c in creeps)
					{
						float xPos = sprite.Position.X + width/2;
						float targetXPos = c.Position.X + c.getWidth/2;
						float yPos = sprite.Position.Y + height/2;
						float targetYPos = c.Position.Y + c.getHeight/2;
						
						float distance = (float)(System.Math.Sqrt((xPos - targetXPos) * (xPos - targetXPos) + (yPos - targetYPos) * (yPos - targetYPos))/50);
						if(distance <= stats.range*2)
						{
							c.inflictSnare(0.55f);
							c.inflictDamage(scene, stats.damage);
						}
					}
				}
				
			}
			fireCooldown-= AppMain.getSpeed();	
		}
		
		public void clearTarget()
		{
			targetCreep = null;
			targetCheck=0;
		}
		
		public BaseCreep getTarget()
		{
			return targetCreep;	
		}
		
		public void sell(Scene scene)
		{
			if(targetCreep != null)
			{
				// [DL] if this has a target, clear it and remove itself from creep
				targetCreep.removeTargettedBy(this);
				targetCreep = null;
			}
			
			// [DL] update Appmain and remove this graphic + texture
			AppMain.depositGold((int)System.Math.Ceiling((float)stats.cost * 0.4f));
			scene.RemoveChild(sprite,true);		
			textureInfo.Dispose();
			
			if(turretTexture != null)
			{
				// [DL] If tower type has a turret, dispose of its texture
				scene.RemoveChild(turretSprite,true);	
				turretTexture.Dispose();
			}
		}
		
		// [DL] Same as sell, but no refund
		public void upgrade(Scene scene)
		{
			if(targetCreep != null)
			{
				// [DL] if this has a target, clear it and remove itself from creep
				targetCreep.removeTargettedBy(this);
				targetCreep = null;
			}
			
			// [DL] update Appmain and remove this graphic + texture
			scene.RemoveChild(sprite,true);		
			textureInfo.Dispose();
			
			if(turretTexture != null)
			{
				// [DL] If tower type has a turret, dispose of its texture
				scene.RemoveChild(turretSprite,true);	
				turretTexture.Dispose();
			}
		}
	}
}

