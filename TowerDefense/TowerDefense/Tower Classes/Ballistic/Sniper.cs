using System;

using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

using Sce.PlayStation.HighLevel.GameEngine2D;
using Sce.PlayStation.HighLevel.GameEngine2D.Base;


namespace TowerDefense
{
	public class Sniper : BaseTower
	{
		private static towerStats originalStats = new towerStats("textures/Towers/Ballistic/sniper.png","Sniper Tower","Snipers use long range rifles to pick off targets",2,4.0f,0.0f,60,10);
		
		public Sniper (Scene scene, Vector2 v) : base(scene, originalStats,v, "Ballistic/sniperTurret")
		{
			
		}
		
		public void towerUpdate()
		{
			
		}
		
		public void fire()
		{
			
		}
	}
}

