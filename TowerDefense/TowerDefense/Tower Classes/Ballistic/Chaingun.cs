using System;

using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

using Sce.PlayStation.HighLevel.GameEngine2D;
using Sce.PlayStation.HighLevel.GameEngine2D.Base;


namespace TowerDefense
{
	public class Chaingun : BaseTower
	{
		private static towerStats originalStats = new towerStats("textures/Towers/Ballistic/chaingun.png","Chaingun","Attacks nearby creeps with devastatingly fast fire",1,1.5f,0.0f,10,6);
		
		public Chaingun (Scene scene, Vector2 v) : base(scene, originalStats,v, "Ballistic/chaingunTurret")
		{
			
		}
		
		public void towerUpdate()
		{
			
		}
		
		public void fire()
		{
			
		}
	}
}

