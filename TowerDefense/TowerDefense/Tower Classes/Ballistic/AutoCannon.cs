using System;

using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

using Sce.PlayStation.HighLevel.GameEngine2D;
using Sce.PlayStation.HighLevel.GameEngine2D.Base;


namespace TowerDefense
{
	public class AutoCannon : BaseTower
	{
		private static towerStats originalStats = new towerStats("textures/Towers/Ballistic/autoCannon.png","AutoCannon","Rapidly attacks nearby creeps with metal pellets",1,1.0f,0.0f,20,2);

		public AutoCannon (Scene scene, Vector2 v) : base(scene, originalStats,v, "Ballistic/autoCannonTurret")
		{

		}
	}
}

