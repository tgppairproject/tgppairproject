using System;

using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

using Sce.PlayStation.HighLevel.GameEngine2D;
using Sce.PlayStation.HighLevel.GameEngine2D.Base;


namespace TowerDefense
{
	public class HeavyArtillery : BaseTower
	{
		private static towerStats originalStats = new towerStats("textures/Towers/Incendiary/heavyArtillery.png","Heavy Artillery","Launches explosive rounds at the target creep",8,2.5f,0.2f,180,10);
		
		public HeavyArtillery (Scene scene, Vector2 v) : base(scene, originalStats,v, "Incendiary/heavyArtilleryTurret")
		{
			
		}
		
		public void towerUpdate()
		{
			
		}
		
		public void fire()
		{
			
		}
	}
}

