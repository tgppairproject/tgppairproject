using System;

using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

using Sce.PlayStation.HighLevel.GameEngine2D;
using Sce.PlayStation.HighLevel.GameEngine2D.Base;


namespace TowerDefense
{
	public class MissileLauncher : BaseTower
	{
		private static towerStats originalStats = new towerStats("textures/Towers/Incendiary/missileLauncher.png","Missile Launcher","Launches missiles",5,3.0f,0.5f,120,6);
		
		public MissileLauncher (Scene scene, Vector2 v) : base(scene, originalStats,v, "Incendiary/MissileLauncherTurret")
		{
			
		}
		
		public void towerUpdate()
		{
			
		}
		
		public void fire()
		{
			
		}
	}
}

