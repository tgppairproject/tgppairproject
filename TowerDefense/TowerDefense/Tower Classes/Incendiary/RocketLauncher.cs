using System;

using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

using Sce.PlayStation.HighLevel.GameEngine2D;
using Sce.PlayStation.HighLevel.GameEngine2D.Base;


namespace TowerDefense
{
	public class RocketLauncher : BaseTower
	{
		private static towerStats originalStats = new towerStats("textures/Towers/Incendiary/rocketLauncher.png","Rocket Launcher","Fires salvos of rockets at creeps",1,2.0f,0.3f,120,8);
		
		public RocketLauncher (Scene scene, Vector2 v) : base(scene, originalStats,v, "Incendiary/rocketLauncherturret")
		{
			
		}
	}
}

