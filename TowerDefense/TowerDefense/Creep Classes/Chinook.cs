using System;
using System.Collections.Generic;

using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

using Sce.PlayStation.HighLevel.GameEngine2D;
using Sce.PlayStation.HighLevel.GameEngine2D.Base;


using System.Diagnostics;

namespace TowerDefense
{
	public class Chinook : BaseCreep
	{
		private creepStats originalStats = new creepStats("textures/Creeps/chinook.png","Chinook","Periodically spawns infantry units while alive",5.0f,5.0f,2.0f,2.0f,15.0f);
		
		private int infantrySpawnTimer;
		
		private SpriteUV bladeSprite1;
		private SpriteUV bladeSprite2;
		private TextureInfo bladeTexture;
		
		public Chinook (Scene scene, int spawnTimer, int MapLevel, Vector2[] waypoints) : base("textures/Creeps/chinook.png", scene, spawnTimer, waypoints)
		{
			// [DL] Create stat modifier depending on map level (tutorial, easy, medium, hard)
			float statModifier = 1.0f+((float)MapLevel * 1.5f);
			originalStats.maxHealth *= statModifier; // [DL] Increase creep health by stat modifier
			originalStats.health = originalStats.maxHealth;
			base.stats=originalStats; // [DL] Call base constructor

			infantrySpawnTimer = 90; // [DL] Set next infantry spawn to 90frames
						
			bladeTexture = new TextureInfo("/Application/textures/Creeps/chinookBlade.png");
			bladeSprite1 = new SpriteUV(bladeTexture);
			bladeSprite2 = new SpriteUV(bladeTexture);
			
			bladeSprite1.Scale = new Vector2(50f,50f);
			bladeSprite2.Scale = new Vector2(50f,50f);
			bladeSprite1.Position = new Vector2(creepSprite.Position.X+12.5f,creepSprite.Position.Y+12.5f);
			bladeSprite2.Position = new Vector2(creepSprite.Position.X-15,creepSprite.Position.Y);
			
			scene.AddChild(bladeSprite1);
			scene.AddChild(bladeSprite2);
		}
		
		protected override void updateClassSpecifics()
		{
			bladeSprite1.Rotate(1);
			bladeSprite2.Rotate(-1);
			bladeSprite1.CenterSprite();
			bladeSprite2.CenterSprite();
			
			if(creepSprite.Angle == 0)
			{
				// [DL] Moving Right
				bladeSprite1.Position = new Vector2(creepSprite.Position.X+15,creepSprite.Position.Y);
				bladeSprite2.Position = new Vector2(creepSprite.Position.X-15,creepSprite.Position.Y);
			}
			else if(creepSprite.Angle <-1.5)
			{
				// [DL] Moving Down	
				bladeSprite1.Position = new Vector2(creepSprite.Position.X,creepSprite.Position.Y-12);
				bladeSprite2.Position = new Vector2(creepSprite.Position.X,creepSprite.Position.Y+15);
			}
			else if(creepSprite.Angle >3)
			{
				// [DL] Moving left
				bladeSprite1.Position = new Vector2(creepSprite.Position.X-15,creepSprite.Position.Y);
				bladeSprite2.Position = new Vector2(creepSprite.Position.X+15,creepSprite.Position.Y);
			}
			else if(creepSprite.Angle >1.5)
			{
				// [DL] Moving up
				bladeSprite1.Position = new Vector2(creepSprite.Position.X,creepSprite.Position.Y+12);
				bladeSprite2.Position = new Vector2(creepSprite.Position.X,creepSprite.Position.Y-15);
			}
		}
		
		protected override void cleanupClassSpecific(Scene scene)
		{
			scene.RemoveChild(bladeSprite1,true);	
			scene.RemoveChild(bladeSprite2,true);	
			bladeTexture.Dispose();
		}
	}
}

