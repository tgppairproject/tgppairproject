using System;
using System.Collections.Generic;

using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

using Sce.PlayStation.HighLevel.GameEngine2D;
using Sce.PlayStation.HighLevel.GameEngine2D.Base;

using System.Reflection;

using System.Diagnostics;

namespace TowerDefense
{
	public class BaseCreep
	{
		protected int spawnTimer; // [DL] Time (frames) until this creep is moved from AppMain.creepQueue to AppMain.creeps
		protected Vector2[] waypoints;
		protected int wpTarget = 0;
		protected Vector2 pos;
		protected float xmod, ymod;
		protected creepStats stats;
		protected bool dead = false;
		protected int snareDuration = 0;
		
		protected SpriteUV creepSprite;
		protected TextureInfo creepTextureInfo;
		private float width;
		private float height;
		
		protected SpriteUV healthRemainingSprite;
		protected TextureInfo healthRemainingTextureInfo;
		protected SpriteUV healthDeficitSprite;
		protected TextureInfo healthDeficitTextureInfo;
		protected float healthWidth;
		
		public Vector2 Position { get { return pos; }}
		public bool isDead { get { return dead; }}
		public float getWidth { get { return width; }}
		public float getHeight { get { return height; }}
		
		protected List<BaseTower> targettedByTower {get; set; }
		protected List<BaseProjectile> targettedByProjectile {get; set; }
		
		
		public BaseCreep (String graphicPath, Scene scene, int spawnTimer, Vector2[] waypoints)
		{
			this.spawnTimer = spawnTimer;
			this.waypoints = waypoints;
			
			targettedByTower = new List<BaseTower>();
			targettedByProjectile = new List<BaseProjectile>();
			
			creepTextureInfo = new TextureInfo("/Application/" + graphicPath);
			creepSprite = new SpriteUV(creepTextureInfo);
			creepSprite.Position = waypoints[0];
						
			creepSprite.Quad.S = creepTextureInfo.TextureSizef;
			Bounds2 b = creepSprite.Quad.Bounds2();
			width = b.Point10.X;
			height = b.Point01.Y;
			
			healthDeficitTextureInfo = new TextureInfo("/Application/textures/Creeps/healthDeficit.png");
			healthDeficitSprite = new SpriteUV(healthDeficitTextureInfo);
			healthRemainingTextureInfo = new TextureInfo("/Application/textures/Creeps/healthRemaining.png");
			healthRemainingSprite = new SpriteUV(healthRemainingTextureInfo);
			healthRemainingSprite.Scale = new Vector2(25f, 2.5f);
			healthDeficitSprite.Scale = new Vector2(25f, 2.5f);
			healthDeficitSprite.Visible = false;
			healthRemainingSprite.Visible = false;
									
			// [DL] Generate x/y offset of 10px, minus 10 to centralize randomization, and an additional 12.5px to centralize due to sprite width/height
			Random r = new Random();
			xmod = AppMain.getRandom().Next(1, 21)-10f -width/2f;
			ymod = AppMain.getRandom().Next(1, 21)-10f -width/2f;
			
			pos = new Vector2(waypoints[0].X + xmod, waypoints[0].Y + ymod);
			creepSprite.Position = pos;
			healthDeficitSprite.Position = new Vector2(pos.X, pos.Y + 25.0f);
			healthRemainingSprite.Position = new Vector2(pos.X, pos.Y + 25.0f);
			
			wpTarget = 1;
			
			creepSprite.CenterSprite();
			healthDeficitSprite.CenterSprite();
			healthRemainingSprite.CenterSprite();
			
			scene.AddChild(creepSprite);
			scene.AddChild(healthDeficitSprite);
			scene.AddChild(healthRemainingSprite);
		}
		
		public void addTargettedBy(BaseTower t)
		{
			targettedByTower.Add (t);	
		}
		
		public void removeTargettedBy(BaseTower t)
		{
			targettedByTower.Remove(t);	
		}
		
		public void removeTargettedBy(BaseProjectile p)
		{
			targettedByProjectile.Remove(p);	
		}
		
		public void setSpawnTimer(int newTimer)
		{
			this.spawnTimer = newTimer;
		}
		
		public int getSpawnTimer()
		{
			return spawnTimer;
		}
		
		public void updateMovement(Scene scene)
		{
			updateClassSpecifics();
			
			// [DL] Calculate x and y range with regards to speed
			float minmaxX = stats.maxSpeed;
			float minmaxY = (ymod - stats.maxSpeed)/2;
			
			if(!dead)
			{
				// [DL] Check if slowed, then apply appropriate Calculate movement x/y speeds and update position
				if(snareDuration<=0)
				{
					pos = (pos -= new Vector2(calculateXMovement(),calculateYMovement()));
				}
				else
				{
					snareDuration--;	
					pos = (pos -= new Vector2(calculateXMovement(),calculateYMovement()));
					
					if(snareDuration<=0)
					{
						// [DL] Was slowed this frame, now isn't. Remove slow effect
						stats.setSpeedModifier(1.0f);
					}
				}
			}
			
			// [DL] Update sprite position
			creepSprite.Position = new Vector2(pos.X-xmod,pos.Y-ymod);
			creepSprite.Angle = (float)System.Math.Atan2((-1*(creepSprite.Position.Y-waypoints[wpTarget].Y)),(-1*(creepSprite.Position.X-waypoints[wpTarget].X)));
			healthDeficitSprite.Position = new Vector2(creepSprite.Position.X,creepSprite.Position.Y+25.0f);
			healthRemainingSprite.Position = new Vector2(creepSprite.Position.X-((25-healthWidth/2)-12.0f),creepSprite.Position.Y+25f);
			
			// [DL] Check this is at target waypoint
			if(((pos.X-xmod) > waypoints[wpTarget].X-1) && ((pos.X-xmod) < waypoints[wpTarget].X+1) && ((pos.Y-ymod) > waypoints[wpTarget].Y-1) && ((pos.Y-ymod) < waypoints[wpTarget].Y+1))
			{
				// [DL] Move the remainder of the way to the waypoint, and update waypoint target
				pos.X =  waypoints[wpTarget].X+xmod;
				pos.Y =  waypoints[wpTarget].Y+ymod;
				wpTarget++;
				
				// [DL] Check if creep has reached end of waypoints
				if(wpTarget >= waypoints.Length)
				{
					wpTarget = 0;
					dead = true;
				}
			}	
			creepSprite.CenterSprite();
		}
		
		private float calculateXMovement()
		{
			var rads = System.Math.Atan2((pos.Y) - (waypoints[wpTarget].Y+ymod), (pos.X) - (waypoints[wpTarget].X+xmod));
			
			float xSpeed = (float)System.Math.Cos(rads) * (stats.maxSpeed * stats.speedModifier * 0.1f);
			return xSpeed;
		}
		
		private float calculateYMovement()
		{
			var rads = System.Math.Atan2((pos.Y) - (waypoints[wpTarget].Y+ymod), (pos.X) - (waypoints[wpTarget].X+xmod));
			
			float ySpeed = (float)System.Math.Sin(rads) * (stats.maxSpeed * stats.speedModifier * 0.1f);
			return ySpeed;
		}
		
		protected virtual void cleanupClassSpecific(Scene scene)
		{
			
		}
		
		public void cleanup(Scene scene)
		{
			// [DL] Remove this creep from any towers targetting this
			foreach(var i in targettedByTower)
			{
				if(i.getTarget() == this)
				{
					i.clearTarget();	
				}
			}
			
			foreach(var i in targettedByProjectile)
			{
				if(i.getTarget() == this)
				{
					i.clearTarget();	
				}
			}
			
			// [DL] Destroy creep
			cleanupClassSpecific(scene);
			scene.RemoveChild(creepSprite,true);
			scene.RemoveChild(healthRemainingSprite,true);
			scene.RemoveChild(healthDeficitSprite,true);		
			creepTextureInfo.Dispose();
			healthRemainingTextureInfo.Dispose();
			healthDeficitTextureInfo.Dispose();
			
			// [DL] Adjust score
			if(stats.health<=0)
			{
				// [DL] Creep was killed, add value/gold to score
				AppMain.addToScore((int)System.Math.Ceiling(stats.maxHealth*stats.maxSpeed));
				AppMain.addCreepKilled();
				AppMain.depositGold((int)System.Math.Ceiling(stats.gold));
			}
			else
			{
				// [DL] Creep didn't die (went off map), subtract value from score
				AppMain.removeFromScore((int)System.Math.Ceiling(stats.health*stats.maxSpeed*5.0f));
				AppMain.addCreepEscaped();
			}
			
		}
		
		protected virtual void updateClassSpecifics()
		{
			// ...
		}
		
		public void inflictDamage(Scene scene, int damage)
		{
			float tempHealth = stats.getHealth() - (float)damage;
			stats.setHealth(tempHealth);
			
			if(stats.health <=0)
			{
				// [DL] Has died
				dead = true;
				cleanup(scene);	
			}
			else
			{
				// [DL] Not dead, but taken damage; update health bar
				healthDeficitSprite.Visible = true;
				healthRemainingSprite.Visible = true;
				healthWidth = 25*(stats.health / stats.maxHealth);
				healthRemainingSprite.Scale = new Vector2(healthWidth,2.5f);
				healthRemainingSprite.Position = new Vector2(creepSprite.Position.X-((25-healthWidth/2)-12.5f),creepSprite.Position.Y+25f);
			}
		}
		
		public void inflictSnare(float modifier)
		{
			stats.setSpeedModifier(modifier);
			snareDuration=60;
		}
	}
}

