using System;
using System.Collections.Generic;

using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

using Sce.PlayStation.HighLevel.GameEngine2D;
using Sce.PlayStation.HighLevel.GameEngine2D.Base;

namespace TowerDefense
{
	public class Militant : BaseCreep
	{
		private creepStats originalStats = new creepStats("textures/Creeps/militant.png","Militant","Foot soldier: Basic Infantry",2.0f,2.0f,3.0f,3.0f,3.0f);
		
		public Militant (Scene scene, int spawnTimer, int MapLevel, Vector2[] waypoints) : base("textures/Creeps/militant.png", scene, spawnTimer, waypoints)
		{
			// [DL] Create stat modifier depending on map level (tutorial, easy, medium, hard)
			float statModifier = 1.0f+((float)MapLevel * 1.5f);
			originalStats.maxHealth *= statModifier; // [DL] Increase creep health by stat modifier
			originalStats.health = originalStats.maxHealth;
			base.stats=originalStats; // [DL] Call base constructor
		}
	}
}

