using System;
using System.Collections.Generic;

using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

using Sce.PlayStation.HighLevel.GameEngine2D;
using Sce.PlayStation.HighLevel.GameEngine2D.Base;

namespace TowerDefense
{
	public class Tank : BaseCreep
	{
		private creepStats originalStats = new creepStats("textures/Creeps/tank.png","Tank","Fire's anti-turret rounds that temporarily stun turrets",35.0f,35.0f,2.0f,2.0f,50.0f);
		
		private SpriteUV turretSprite;
		private TextureInfo turretTexture;
		
		public Tank (Scene scene, int spawnTimer, int MapLevel, Vector2[] waypoints) : base("textures/Creeps/tank.png", scene, spawnTimer, waypoints)
		{
			// [DL] Create stat modifier depending on map level (tutorial, easy, medium, hard)
			float statModifier = 1.0f+((float)MapLevel * 1.5f);
			originalStats.maxHealth *= statModifier; // [DL] Increase creep health by stat modifier
			originalStats.health = originalStats.maxHealth;
			base.stats=originalStats; // [DL] Call base constructor
			
			turretTexture = new TextureInfo("/Application/textures/Creeps/tankTurret.png");
			turretSprite = new SpriteUV(turretTexture);
			turretSprite.Scale = new Vector2(50f,50f);
			
			turretSprite.Position = new Vector2(creepSprite.Position.X,creepSprite.Position.Y);
			
			scene.AddChild(turretSprite);
		}
		
		protected override void updateClassSpecifics()
		{
			//turretSprite.Scale = new Vector2(50f,50f);
			turretSprite.Angle = creepSprite.Angle;
			turretSprite.CenterSprite();
			turretSprite.Position = new Vector2(creepSprite.Position.X,creepSprite.Position.Y);
		}
		
		protected override void cleanupClassSpecific(Scene scene)
		{
			scene.RemoveChild(turretSprite,true);		
			turretTexture.Dispose();
		}
	}
}

