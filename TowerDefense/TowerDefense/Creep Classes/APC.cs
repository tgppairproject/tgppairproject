using System;
using System.Collections.Generic;

using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

using Sce.PlayStation.HighLevel.GameEngine2D;
using Sce.PlayStation.HighLevel.GameEngine2D.Base;

namespace TowerDefense
{
	public class APC : BaseCreep
	{
		protected creepStats originalStats = new creepStats("textures/Creeps/apc.png","APC","Periodically spawns infantry units while alive",15.0f,15.0f,2.0f,2.0f,10.0f);
		
		protected int infantrySpawnTimer;
		
		public APC (Scene scene, int spawnTimer, int MapLevel, Vector2[] waypoints) : base("textures/Creeps/apc.png", scene, spawnTimer, waypoints)
		{
			// [DL] Create stat modifier depending on map level (tutorial, easy, medium, hard)
			float statModifier = 1.0f+((float)MapLevel * 1.5f);
			originalStats.maxHealth *= statModifier; // [DL] Increase creep health by stat modifier
			originalStats.health = originalStats.maxHealth;
			base.stats=originalStats; // [DL] Call base constructor
			
			infantrySpawnTimer = 90; // [DL] Set next infantry spawn to 90frames
		}
	}
}

