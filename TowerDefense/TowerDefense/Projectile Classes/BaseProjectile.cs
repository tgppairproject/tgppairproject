using System;
using System.Collections.Generic;

using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

using Sce.PlayStation.HighLevel.GameEngine2D;
using Sce.PlayStation.HighLevel.GameEngine2D.Base;

using System.Diagnostics;

namespace TowerDefense
{
	public class BaseProjectile
	{
		protected BaseCreep targetCreep;
		protected Vector2 creepPos;
		protected int damage;
		protected bool trackTarget = true;
		protected bool hit = false;
		
		public bool hasHit { get { return hit; }}
		
		protected SpriteUV sprite;
		protected TextureInfo texture;
		
		public BaseProjectile (Scene scene, Vector2 pos, BaseCreep c, int d, String fileName)
		{
			targetCreep = c;
			texture = new TextureInfo("/Application/textures/Projectiles/" + fileName + ".png");
			sprite = new SpriteUV(texture);
			sprite.Position = new Vector2(pos.X+25f,pos.Y+25f);
			
			this.damage = d;
			
			if(this is BulletProjectile)
			{
				sprite.Scale = new Vector2(5f,5f);
			}
			else
			{
				sprite.Scale = new Vector2(25f,25f);
			}
			scene.AddChild(sprite);
		}
		
		public void updateMovement(Scene scene)
		{
			// [DL] If a target is active and being tracked...
			if(trackTarget)
			{
				// [DL] Check if reached target
				if((sprite.Position.X > targetCreep.Position.X+(targetCreep.getWidth/2)-5)
				   && (sprite.Position.X < targetCreep.Position.X+(targetCreep.getWidth/2)+5)
				   && (sprite.Position.Y > targetCreep.Position.Y+(targetCreep.getHeight/2)-5)
				   && (sprite.Position.Y < targetCreep.Position.Y+(targetCreep.getHeight/2)+5))
				{
					// [DL] Reached target
					hit=true;
				}
				else
				{
					// [DL] Move towards target
					sprite.Position = new Vector2(sprite.Position.X-3f*(calculateXMovement(targetCreep.Position)),sprite.Position.Y-3f*(calculateYMovement(targetCreep.Position)));
					sprite.Rotation = new Vector2(-1 * calculateXMovement(targetCreep.Position),-1 * calculateYMovement(targetCreep.Position));
					sprite.CenterSprite();
				}
			}
			else
			{
				// [DL] No target tracked, move towards old coordinates
				if((sprite.Position.X > creepPos.X-5)
				   && (sprite.Position.X < creepPos.X+5)
				   && (sprite.Position.Y > creepPos.Y-5)
				   && (sprite.Position.X < creepPos.X+5))
				{
					// [DL] At old creep position, see if anything to hit
					
				}
				else
				{
					// [DL] Move towards target
					sprite.Position = new Vector2(sprite.Position.X-calculateXMovement(creepPos),sprite.Position.Y-calculateYMovement(creepPos));
					sprite.Angle = (float)System.Math.Atan2((sprite.Position.Y-targetCreep.Position.Y),(sprite.Position.X-targetCreep.Position.X));
				}
			}
		}
		
		public void processSplash(Scene scene, List<BaseCreep> creeps)
		{
			// [DL] Apply splash damage to all targets in range
			foreach(BaseCreep c in creeps)
			{
				// [DL] Check distance
				float xPos = sprite.Position.X;
				float targetXPos = c.Position.X;
				float yPos = sprite.Position.Y;
				float targetYPos = c.Position.Y;
				
				float distance = (float)(System.Math.Sqrt((xPos - targetXPos) * (xPos - targetXPos) + (yPos - targetYPos) * (yPos - targetYPos))/50);
				
				if(distance <= 1.0f)
				{
					// [DL] Check that you arn't applying splash to the target creep which gets hit in impact()
					if(c != targetCreep)
					{
						// [DL] If in splash range, apply damage based on distance to epi-center
						float tempDamage = damage * (1.0f-(distance / 1.0f));
						c.inflictDamage(scene, (int)System.Math.Ceiling(tempDamage));
					}
					
				}
			}	
		}
		
		public void cleanup(Scene scene)
		{
			// [DL] Remove this projectile from any creeps targettedBy list
			if(targetCreep != null)
			{
				// [DL] If this has a target, Remove this projectile from its targettedBy list
				targetCreep.removeTargettedBy(this);
			}
			
			// Add code to clear this creep from any projectiles targetting this
			
			// [DL] Destroy creep
			AppMain.removeProjectile(this);
			scene.RemoveChild(sprite,true);		
			texture.Dispose();
		}
		
		// [DL] Normal clean up must remove itself from AppMain projectiles list, this one doesnt
		public void partialCleanup(Scene scene)
		{
			// [DL] Remove this projectile from any creeps targettedBy list
			if(targetCreep != null)
			{
				// [DL] If this has a target, Remove this projectile from its targettedBy list
				targetCreep.removeTargettedBy(this);
			}
			
			// Add code to clear this creep from any projectiles targetting this
			
			// [DL] Destroy creep
			scene.RemoveChild(sprite,true);		
			texture.Dispose();
		}
		
		public void clearTarget()
		{
			targetCreep = null;
			trackTarget = false;
		}
		
		public BaseCreep getTarget()
		{
			return targetCreep;	
		}
		
		public void impact(Scene scene, List<BaseCreep> creeps)
		{
			// [DL] Add damage infliction, snare infliction, destroy textures and remove from projectile list
			targetCreep.inflictDamage(scene, damage);
			
			if(!(this is BulletProjectile))
			{
				// [DL] If this isn't a bullet, projectile must be incendiary, apply splash and explosion
				processSplash(scene, creeps);
				IncendiaryParticle p = new IncendiaryParticle(new Vector2(sprite.Position.X,sprite.Position.Y),scene);
				AppMain.addIncendiaryParticle(p);
			}
			
			cleanup(scene);
		}
		
		private float calculateXMovement(Vector2 v)
		{
			var targ = v;
			
			if(targetCreep!=null)
			{
				// [DL] If this has a target, adjust target location to include their width/2 and height/2
				targ = new Vector2(v.X + targetCreep.getWidth/2, v.Y + targetCreep.getHeight/2);
			}
			
			var rads = System.Math.Atan2((sprite.Position.Y - (targ.Y)), (sprite.Position.X - (targ.X)));
			
			float xSpeed = (float)System.Math.Cos(rads);
			return xSpeed;
		}
		
		private float calculateYMovement(Vector2 v)
		{
			var targ = v;
			
			if(targetCreep!=null)
			{
				// [DL] If this has a target, adjust target location to include their width/2 and height/2
				targ = new Vector2(v.X + targetCreep.getWidth/2, v.Y + targetCreep.getHeight/2);
			}
			
			var rads = System.Math.Atan2((sprite.Position.Y - targ.Y), (sprite.Position.X - targ.X));
			
			float ySpeed = (float)System.Math.Sin(rads);
			return ySpeed;
		}
	}
}

