using System;

using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

using Sce.PlayStation.HighLevel.GameEngine2D;
using Sce.PlayStation.HighLevel.GameEngine2D.Base;

namespace TowerDefense
{
	public class RocketProjectile : BaseProjectile
	{
		public RocketProjectile (Scene scene, Vector2 pos, BaseCreep c, int d, String fileName) :base(scene, pos, c, d, fileName)
		{
			
		}
	}
}

