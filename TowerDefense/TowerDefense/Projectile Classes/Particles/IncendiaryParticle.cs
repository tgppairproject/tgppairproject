using System;
using System.Collections.Generic;

using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

using Sce.PlayStation.HighLevel.GameEngine2D;
using Sce.PlayStation.HighLevel.GameEngine2D.Base;

using System.Diagnostics;

namespace TowerDefense
{
	public class IncendiaryParticle
	{
		private bool sizeIncreasing = true;
		private float size;
		private float originalSize = 25f;
		private bool finished = false;
		
		private TextureInfo texture;
		private SpriteUV sprite;
		
		public bool hasFinished { get { return finished; }}
		
		public IncendiaryParticle (Vector2 v, Scene scene)
		{
			size = 0.0f;
			
			texture = new TextureInfo("/Application/textures/Projectiles/explosion.png");
			sprite = new SpriteUV(texture);
			sprite.Scale = new Vector2(0f,0f);
			sprite.Position = v;
			
			scene.AddChild(sprite);
		}
		
		public void UpdateState(Scene scene)
		{
			if(sizeIncreasing && size<originalSize)
			{
				// [DL] Increase size whilst size is under originalSize	
				size = size + 1*AppMain.getSpeed();
				sprite.Scale = new Vector2(25*(size/originalSize),25*(size/originalSize));
				sprite.CenterSprite();
			}
			else if(sizeIncreasing && size>=originalSize)
			{
				// [DL] Reached max size
				sizeIncreasing = false;
				
				size = size - 1*AppMain.getSpeed();
				sprite.Scale = new Vector2(25*(size/originalSize),25*(size/originalSize));
				sprite.CenterSprite();
			}
			else if(!sizeIncreasing && size>0)
			{
				// [DL] Decrease in size whilst size is over 0
				size = size - 3*AppMain.getSpeed();
				sprite.Scale = new Vector2(25*(size/originalSize),25*(size/originalSize));
				sprite.CenterSprite();
			}
			else if(!sizeIncreasing && size<=0)
			{
				// [DL] Has reached 0 again, cleanup
				finished = true;
				cleanUp(scene);
			}
		}
		
		public void cleanUp(Scene scene)
		{
			scene.RemoveChild(sprite,true);
			texture.Dispose();
		}
	}
}

