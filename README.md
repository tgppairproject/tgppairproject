# README #

### What is this repository for? ###

University: Staffordshire University

Course: BSc(hons) Computer Games Programming

Module: Technical Games Production



Project Details;


Professional development of a game, using Trello for task management, and Bitbucket for Version Control


Following set coding standards (comments in particular) 

### How do I get set up? ###

1) You'll need the PSM SDK -> Download Link: https://psm.playstation.net/static/general/all/en/psm_sdk.html


2) Open the Game Solution in PSM - Play using the PSM Simulator or Download to a PS Vita for the best game experience



This game remains property of Staffordshire University

Developed by Daniel Lockyer and Richard Boyd

### Contribution guidelines ###

Private Academic Project